import logging
import json
import yaml

# Configure logging
logging.basicConfig(format='%(message)s',)
# logging.basicConfig(format='%(levelname)-8s: %(message)s ',)
logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')


if __name__ == "__main__":
    info_file = '/tmp/collaborator_info.sorted.json'
    with open(info_file) as fp:
        user_info = json.load(fp)

    roles = {}
    working_groups = {}
    users_in_wgs = []
    users_with_roles = []
    for user in user_info:
        name = user['name']
        email = user['emails']['email']
        try:
            role = user['keycloak']['profile_attributes']['role']
        except KeyError:
            # logger.warning(f'''User has no role: "{name}": {email}''')
            continue
        # if not role:
        #     logger.warning(f'''User has no role: "{name}": {email}''')
        if len(role) > 1:
            logger.warning(f'''User "{name}" has multiple roles!''')
        for role_label in role:
            if role_label in roles:
                roles[role_label].append(name)
            else:
                roles[role_label] = [name]
            if name not in users_with_roles:
                users_with_roles.append(name)
        try:
            working_group_list = user['keycloak']['profile_attributes']['working_group']
        except KeyError:
            # logger.warning(f'''User has no working groups: "{name}": {email}''')
            continue
        # if not working_group_list:
        #     logger.warning(f'''User has no working groups: "{name}": {email}''')
        for working_group in working_group_list:
            if working_group in working_groups:
                working_groups[working_group].append(name)
            else:
                working_groups[working_group] = [name]
            if name not in users_in_wgs:
                users_in_wgs.append(name)

    logger.info(f'''Number of collaborators: {len(user_info)}\n''')
    logger.info('''\n''')
    logger.info('''Roles:\n''')
    all_roles = 0
    for role_label in roles:
        all_roles += len(roles[role_label])
        logger.info(f'''{role_label}: {len(roles[role_label])}''')
    logger.info('''\n''')
    logger.info(f'''Total people with roles: {all_roles}''')
    users_not_in_roles = [
        f'''{user['name']}: {user['emails']['email']}'''
        for user in user_info
        if user['name'] not in users_with_roles]
    logger.info(f'''{len(users_not_in_roles)} people WITHOUT a role:\n{yaml.dump(users_not_in_roles)}''')

    logger.info('''\n''')
    logger.info('''Working groups:\n''')
    for working_group_label in working_groups:
        logger.info(f'''{working_group_label}: {len(working_groups[working_group_label])}''')
    logger.info(f'''Total people in working_groups: {len(users_in_wgs)}''')

    # users_not_in_wg = [name for name in [user['name'] for user in user_info] if name not in users_in_wgs]
    users_not_in_wg = [
        f'''{user['name']}: {user['emails']['email']}'''
        for user in user_info
        if user['name'] not in users_in_wgs]
    logger.info(f'''{len(users_not_in_wg)} people NOT in working_groups:\n{yaml.dump(users_not_in_wg)}''')

    csv_lines = ['''Name\tRole\tWorking groups\tAffiliation\n''']
    # csv_lines = ['''Name\tEmail\tRole\tWorking groups\tAffiliation\n''']
    for user in user_info:
        name = user['name']
        email = user['emails']['email']
        try:
            role = ', '.join(user['keycloak']['profile_attributes']['role'])
        except KeyError:
            role = ''
        try:
            working_group = ', '.join(user['keycloak']['profile_attributes']['working_group'])
        except KeyError:
            working_group = ''
        try:
            affiliation = ', '.join(user['keycloak']['profile_attributes']['affiliation'])
        except KeyError:
            affiliation = ''
        csv_lines.append(f'''{name}\t{role}\t{working_group}\t{affiliation}\n''')
        # csv_lines.append(f'''{name}\t{email}\t{role}\t{working_group}\t{affiliation}\n''')
    with open('/tmp/collaborator_list.csv', 'w') as fh:
        fh.writelines(csv_lines)
