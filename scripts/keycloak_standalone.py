import requests
import os, sys
import logging
from urllib import parse
import json
import yaml

# Configure logging
logging.basicConfig(
    format='%(message)s ',
    # format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger(__name__)
log.setLevel('DEBUG')
# handler = logging.StreamHandler(sys.stdout)
# handler.setLevel(logging.DEBUG)
# log.addHandler(handler)

class KeycloakApi():
    ## ref: https://www.keycloak.org/docs-api/15.0/rest-api/#_users_resource
    def __init__(self):
        self.auth_token = ''
        self.refresh_token = ''
        self.base_url = os.environ.get('KEYCLOAK_BASE_URL', "https://keycloak.muses.ncsa.illinois.edu/admin/realms/MUSES")
        self.username = os.environ.get('KEYCLOAK_USER', '')
        self.password = os.environ.get('KEYCLOAK_PASS', '')
        self.client_id = os.environ.get('KEYCLOAK_CLIENT_ID', "admin-cli")
        self.client_secret = os.environ.get('KEYCLOAK_CLIENT_SECRET', "")
        if self.client_id == 'admin-cli':
            self.login_url = os.environ.get('KEYCLOAK_LOGIN_URL', "https://keycloak.muses.ncsa.illinois.edu/realms/master/protocol/openid-connect/token")
        else:
            self.login_url = os.environ.get('KEYCLOAK_LOGIN_URL', "https://keycloak.muses.ncsa.illinois.edu/realms/MUSES/protocol/openid-connect/token")
        self.login()

    def login(self):
        # Login to obtain an auth token
        data = {
            'grant_type': 'password',
            'client_id': self.client_id,
            'username': self.username,
            'password': self.password,
        }
        # if self.client_secret:
        # data['client_secret'] = self.client_secret
        log.debug(f'''Logging in to "{self.login_url}"''')
        r = requests.request( 'POST',
            self.login_url,
            data=data,
        )
        # Store the JWT auth token
        try:
            assert r.status_code == 200
            response = r.json()
            log.debug(json.dumps(response, indent=2))
            self.auth_token = response['access_token']
            self.refresh_token = response['refresh_token']
        except:
            log.error(f'''Error authenticating user "{self.username}". Exiting.''')
            log.error(r.status_code)
            log.error(r.text)

    def get_user_attributes(self, email=''):
        try:
            user_info = self.get_user_info(email=email)[0]
            return user_info['attributes']
        except Exception as e:
            log.error(f'''Error getting user attributes: {e}''')
        return []

    def update_user_attributes(self, user_id='', attributes={}):
        assert user_id
        response = requests.request( 'PUT',
            f'''{self.base_url}/users/{user_id}''',
            json={
                'attributes': attributes,
            },
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            success = True
        except Exception as e:
            success = False
            log.error(f'''Error updating user attributes: {e}''')
        return success

    def get_user_info(self, email=''):
        response = requests.request( 'GET',
            f'''{self.base_url}/users''',
            params={
                'email': email,
                'exact': True,
                'enabled': True,
            },
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except Exception as e:
            log.error(f'''Error getting user info: {e}, {response.text}''')
        return response

    def apply_role_mapping_to_user(self, user_id, role_id='', role_name='', realm='MUSES'):
        assert role_id and role_name and realm
        params = [{
            "id": role_id,
            "name": role_name,
            "description": "{"+role_name+"}",
            "composite": False,
            "clientRole": False,
            "containerId": realm,
        }]

        r = requests.request( 'POST',
            f'''{self.base_url}/users/{user_id}/role-mappings/realm''',
            json=params,
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert r.status_code in [200, 204]
            success = True
        except:
            success = False
            log.error(f'''Error adding "{user_id}" to role "{role_name}".''')
            log.error(r.status_code)
            log.error(r.text)
        return success

    def get_user_role_mapping(self, user_id, realm='MUSES'):
        assert user_id and realm
        response = requests.request( 'GET',
            f'''{self.base_url}/users/{user_id}/role-mappings''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''Error getting "{user_id}" realm role mappings.''')
            log.error(f'''[{response.status_code}] {response.text}''')
        return response

    def get_default_profile_attributes(self):
        return {
            'affiliation',
            'author_name',
            'website',
            'working_group',
            'role',
            'acknowledgements',
        }

    def get_profile_attributes(self, email):
        attributes = {}
        profile_attributes = self.get_default_profile_attributes()
        try:
            keycloak_user = self.get_user_info(email=email)
            if not keycloak_user:
                return None
            keycloak_user = keycloak_user[0]
            if 'attributes' not in keycloak_user:
                return {}
            for attribute in keycloak_user['attributes']:
                if attribute not in profile_attributes:
                    continue
                attributes[attribute] = []
                for value in keycloak_user['attributes'][attribute]:
                    attributes[attribute].append(parse.unquote_plus(value))
            return attributes
        except Exception as e:
            log.error(f'Error getting user attributes: {e}')
            return None

    def urlencode_profile_attributes(self, attributes):
        encoded_attributes = {}
        for attribute in attributes:
            encoded_attributes[attribute] = []
            for value in attributes[attribute]:
                encoded_attributes[attribute].append(parse.quote_plus(value.strip()))
        return encoded_attributes

    def update_profile_attribute(self, attribute, value, scalar):
        if attribute in profile_attributes:
            ## TODO: What if the attribute is not a list? Can this occur?
            if isinstance(profile_attributes[attribute], list):
                if value in profile_attributes[attribute]:
                    log.info(f'''Attribute "{attribute}: {value}" already exists. No update required.''')
                    return
                else:
                    if scalar:
                        profile_attributes[attribute] = [value]
                        log.debug(f'''Updating attribute "{attribute}: {value}"...''')
                    else:
                        ## Append value to existing attribute.
                        profile_attributes[attribute].append(value)
                        log.debug(f'''Adding "{value}" to attribute "{attribute}"...''')
            else:
                return
        else:
            ## Add attribute if it does not exist. Assume it is multi-valued.
            profile_attributes[attribute] = [value]
            log.debug(f'''Creating attribute "{attribute}: {value}"...''')
        self.update_user_attributes(user_id=user_id, attributes=self.urlencode_profile_attributes(profile_attributes))

def compile_collaborator_info_with_profile_attributes():
    # log.debug(json.dumps(api.get_user_attributes(email='manninga@illinois.edu'), indent=2))

    api = KeycloakApi()

    with open('/tmp/collaborator_emails.json', 'r') as infile:
        personnel_info = json.load(infile)

    personnel_info_with_attributes = []
    for member in personnel_info:
        name=member['name']
        try:
            email=member['emails']['email']
            keycloak_user = api.get_user_info(email=email)[0]
        except IndexError:
            email=member['emails']['associated_accounts'][0]["description"]
            keycloak_user = api.get_user_info(email=email)[0]
        log.debug(f'''{name} ({email})''')
        profile_attributes = api.get_profile_attributes(email=email)
        log.debug(f'''Keycloak User:\n{json.dumps(keycloak_user, indent=2)}''')
        # log.debug(f'''Keycloak User: {keycloak_user['lastName']}, {keycloak_user['firstName']}''')
        user_id = keycloak_user['id']
        log.debug(f'''Attributes:\n{json.dumps(profile_attributes, indent=2)}''')
        member['keycloak'] = {
            'id': user_id,
            'profile_attributes': profile_attributes,
        }
        personnel_info_with_attributes.append(member)

    with open('/tmp/collaborator_info.json', 'w') as outfile:
        personnel_info_with_attributes_sorted = sorted(personnel_info_with_attributes, key=lambda member: member['id'])
        personnel_info = json.dump(personnel_info_with_attributes_sorted, outfile, indent=2)

def update_profile_attributes_from_info_file():

    with open('personnel_info_importer/collaborator_info.revised_emails.json', 'r') as infile:
        personnel_info = json.load(infile)

    for member in personnel_info:
        log.debug(f'''\nMember: {member['Name']}''')
        email=member['Email']
        # if not email == 'cratti@uh.edu':
        #     continue
        keycloak_user = api.get_user_info(email=email)
        # log.debug(keycloak_user)
        profile_attributes = api.get_profile_attributes(email=email)
        keycloak_user = keycloak_user[0]
        log.debug(f'''Keycloak User: {keycloak_user['lastName']}, {keycloak_user['firstName']}''')
        # log.debug(f'''Keycloak User:\n{json.dumps(keycloak_user, indent=2)}''')
        user_id = keycloak_user['id']
        log.debug(f'''Attributes:\n{json.dumps(profile_attributes, indent=2)}''')
        if profile_attributes:
            log.debug(f'''WARNING: Attributes already exist!''')

        if 'Acknowledgements' in member:
            if not member['Acknowledgements']:
                continue
            api.update_profile_attribute(attribute='acknowledgements', value=member['Acknowledgements'], scalar=True)
        if 'Webpage' in member:
            if not member['Webpage']:
                continue
            api.update_profile_attribute(attribute='website', value=member['Webpage'], scalar=True)
        if 'Affiliation' in member:
            if not member['Affiliation']:
                continue
            api.update_profile_attribute(attribute='affiliation', value=member['Affiliation'], scalar=False)
        if 'Role' in member:
            role = member['Role'].lower()
            if role in ['postdoc', 'post doc']:
                role = 'postdoctoral researcher'
            elif role in ['faculty', 'professor', 'research professor'] or role.endswith('professor'):
                role = 'faculty'
            elif role in ['graduate student', 'grad student']:
                role = 'graduate student'
            elif role in ['undergraduate student', 'undergrad student', 'undergrad', 'undergraduate']:
                role = 'undergraduate student'
            elif role in ['research scientist']:
                role = 'research scientist'
            elif role in ['principal investigator']:
                role = 'principal investigator'
            elif role in ['senior investigator']:
                role = 'senior investigator'
            elif role in ['co-principal investigator']:
                role = 'co-principal investigator'
            elif role in ['other']:
                role = 'other'
            else:
                log.debug(f'''ERROR: Unrecognized role: {role}''')
                raise ValueError
                continue
            api.update_profile_attribute(attribute='role', value=role, scalar=True)

        if 'Working Groups' in member:
            log.debug(f'''working groups: {member['Working Groups']}''')
            working_groups = member['Working Groups'].lower().split(',')
            working_groups = [wg.strip() for wg in working_groups]
            revised_working_groups = []
            if 'users' in working_groups:
                revised_working_groups.append('users')
            if 'cyberinfrastructure' in working_groups:
                revised_working_groups.append('cyberinfrastructure')
            if 'eos: neutron stars' in working_groups:
                revised_working_groups.append('neutron stars')
            if 'eos: heavy ion' in working_groups:
                revised_working_groups.append('heavy ion')
            if 'all' in working_groups:
                revised_working_groups = ['heavy ion', 'neutron stars', 'cyberinfrastructure', 'users']
            for wg in revised_working_groups:
                api.update_profile_attribute(attribute='working_group', value=wg, scalar=False)

if __name__ == "__main__":

    api = KeycloakApi()
    # profile_attributes = api.get_profile_attributes('manninga@illinois.edu')
    # log.info(yaml.dump(profile_attributes, indent=2))

    # profile_attributes = api.get_profile_attributes('mateusreinke@hotmail.com')
    # log.info(yaml.dump(profile_attributes, indent=2))

    # compile_collaborator_info_with_profile_attributes()
    with open('/tmp/collaborator_info.json', 'r') as fp:
        personnel_info = json.load(fp)
    with open('/tmp/collaborator_info.sorted.json', 'w') as fp:
        json.dump(sorted(personnel_info, key=lambda member: member['id']), fp, indent=2)
    sys.exit(0)
