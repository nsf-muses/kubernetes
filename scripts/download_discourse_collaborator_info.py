import os
import json
import requests

def get_collaborator_info():
    r = requests.request( 'GET',
        f'''https://musesframework.io/api/v1/export/collaborators/emails''',
        headers={
            'Authorization': f'''Bearer {os.environ['MUSES_API_TOKEN']}''',
        },
    )
    assert r.status_code == 200
    response = r.json()
    return response

def download_collaborator_info():
    collaborator_info = get_collaborator_info()
    with open('/tmp/collaborator_emails.json', 'w') as outfile:
        json.dump(collaborator_info, outfile, indent=2)
    idx = 0
    for member in collaborator_info:
        idx += 1
        print(f'''{idx}: {member['name']} <{member['emails']['email']}>''')
    for member in collaborator_info:
        print(f'''{member['name']} <{member['emails']['email']}>, ''', end='')

if __name__ == "__main__":
    download_collaborator_info()

