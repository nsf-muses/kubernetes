import requests
import os
import sys
import logging
from urllib import parse
import json
import yaml


'''
This script is an example of how a non-admin Keycloak user

- with assigned role "realm-management:view-users"
- who has created a password via https://keycloak.muses.ncsa.illinois.edu/realms/MUSES/account/#/security/signingin

can use the Keycloak REST API to fetch information about users such as their profile data.

Set environment variables:

$ export KEYCLOAK_USER=YOUR_KEYCLOAK_USERNAME
$  export KEYCLOAK_PASS=YOUR_KEYCLOAK_PASSWORD ## leading space prevents password appearing in shell command history
$ export LOG_LEVEL=INFO/DEBUG/etc

Run script with user email address:

$ python get_keycloak_user_profile.py USER@EXAMPLE.COM

affiliation:
- Department of Physics, ...
author_name:
- Arthur Pendragon
role:
- postdoctoral researcher
website:
- arthur.example.com
working_group:
- neutron stars
- cyberinfrastructure
'''


# Configure logging
logging.basicConfig(format='%(message)s ')
log = logging.getLogger(__name__)
log.setLevel(os.environ.get('LOG_LEVEL', logging.DEBUG))


class KeycloakApi():
    # ref: https://www.keycloak.org/docs-api/22.0.5/rest-api/index.html#_users
    def __init__(self):
        self.auth_token = ''
        self.refresh_token = ''
        self.base_url = os.environ.get('KEYCLOAK_BASE_URL', "https://keycloak.muses.ncsa.illinois.edu/admin/realms/MUSES")
        self.username = os.environ.get('KEYCLOAK_USER', '')
        self.password = os.environ.get('KEYCLOAK_PASS', '')
        self.client_id = os.environ.get('KEYCLOAK_CLIENT_ID', "admin-cli")
        self.client_secret = os.environ.get('KEYCLOAK_CLIENT_SECRET', "")
        if self.username == 'user':
            self.login_url = os.environ.get('KEYCLOAK_LOGIN_URL', "https://keycloak.muses.ncsa.illinois.edu/realms/master/protocol/openid-connect/token")
        else:
            self.login_url = os.environ.get('KEYCLOAK_LOGIN_URL', "https://keycloak.muses.ncsa.illinois.edu/realms/MUSES/protocol/openid-connect/token")
        self.login()

    def login(self):
        # Login to obtain an auth token
        data = {
            'grant_type': 'password',
            'client_id': self.client_id,
            'username': self.username,
            'password': self.password,
        }
        # if self.client_secret:
        data['client_secret'] = self.client_secret
        log.debug(f'''Logging in as "{self.username}" to "{self.login_url}"''')
        r = requests.request('POST',
            self.login_url,
            data=data,
        )
        # Store the JWT auth token
        try:
            assert r.status_code == 200
            response = r.json()
            log.debug(json.dumps(response, indent=2))
            self.auth_token = response['access_token']
            self.refresh_token = response['refresh_token']
        except:
            log.error(
                f'''Error authenticating user "{self.username}". Exiting.''')
            log.error(r.status_code)
            log.error(r.text)

    def get_user_info(self, email=''):
        response = requests.request('GET',
            f'''{self.base_url}/users''',
            params={
                'email': email,
                'exact': True,
                'enabled': True,
            },
            headers={'Authorization': 'Bearer {}'.format(
                self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except Exception as e:
            log.error(f'''Error getting user info: {e}, {response.text}''')
        return response

    def get_default_profile_attributes(self):
        return {
            'affiliation',
            'author_name',
            'website',
            'working_group',
            'role',
            'acknowledgements',
        }

    def get_profile_attributes(self, email):
        attributes = {}
        profile_attributes = self.get_default_profile_attributes()
        try:
            keycloak_user = self.get_user_info(email=email)
            if not keycloak_user:
                return None
            keycloak_user = keycloak_user[0]
            if 'attributes' not in keycloak_user:
                return {}
            for attribute in keycloak_user['attributes']:
                if attribute not in profile_attributes:
                    continue
                attributes[attribute] = []
                for value in keycloak_user['attributes'][attribute]:
                    attributes[attribute].append(parse.unquote_plus(value))
            return attributes
        except Exception as e:
            log.error(f'Error getting user attributes: {e}')
            return None


if __name__ == "__main__":

    api = KeycloakApi()

    profile_attributes = api.get_profile_attributes(sys.argv[1])
    log.info(yaml.dump(profile_attributes, indent=2))

    sys.exit(0)
