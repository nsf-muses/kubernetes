from discourse import DiscourseApi
import yaml, json
import logging
import os, sys
import csv
from html.parser import HTMLParser

# Configure logging
logging.basicConfig(format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
log = logging.getLogger("discourse-api-client")
log.setLevel('DEBUG')

config = {
    'discourse': {
        'apiUser': os.environ.get('DISCOURSE_API_USER', 'andrew.manning'),
        'apiKey': os.environ.get('DISCOURSE_API_KEY', ''),
        'base_url': os.environ.get('DISCOURSE_BASE_URL', 'https://forum.musesframework.io'),
    },
    'outputBasePath':  os.environ.get('OUTPUT_BASE_PATH', '.')
}

if not config['discourse']['apiKey']:
    log.fatal(f'''You must first set an API token:\n    export DISCOURSE_API_KEY=*****\nAborting.''')
    sys.exit()

class MyHTMLParser(HTMLParser):
    '''Parse the specific HTML table structure of the personnel table'''
    def __init__(self):
        super().__init__()
        self.labels = []
        self.parsing = ''
        self.col_idx = -1
        self.personnel = []
        self.url = ''
        self.table = ''
        self.institutions = []

    def handle_starttag(self, tag, attrs):
        # log.debug(f"Encountered a start tag: {tag}")
        if tag == 'th':
            ## If a row was perviously parsed, then we must be starting a new table.
            if self.parsing == 'row':
                log.debug(f'''Resetting labels: {self.labels}...''')
                self.labels = []
            self.parsing = 'label'
        elif tag == 'tr':
            self.parsing = 'row'
            self.col_idx = -1
            self.row_data = {}
        elif tag == 'td':
            self.parsing = 'cell'
            self.col_idx += 1
        elif tag == 'a' and self.parsing == 'cell':
            # log.debug(f'''{attrs}''')
            self.url = [attr[1] for attr in attrs if attr[0] == 'href'][0]

    def handle_endtag(self, tag):
        # log.debug(f"Encountered an end tag: {tag}")
        if tag in ['td', 'th', 'tr']:
            self.parsing = ''
        # if tag == 'th':
        #     else:
        #         self.table = ''
        if tag == 'tr' and self.row_data:
            self.url = ''
            if self.table == 'institution':
                self.institutions.append(self.row_data)
            elif self.table == 'personnel':
                self.personnel.append(self.row_data)

    def handle_data(self, data):
        # log.debug(f"Encountered some data: {data}")
        if self.parsing == 'label':
            self.labels.append(data)
            if 'Institution' in self.labels:
                log.debug('Parsing affiliated institution table...')
                self.table = 'institution'
            if 'Name' in self.labels:
                log.debug('Parsing personnel table...')
                self.table = 'personnel'
            log.debug(f'''self.table: {self.table}''')
            log.debug(f'''Column labels: {self.labels}''')
        if self.parsing == 'cell':
            label = self.labels[self.col_idx]
            if data == '\n':
                return
            # log.debug(f'''[{label}] {data}''')
            if label == 'Webpage':
                self.row_data[label] = self.url
            elif label == 'Affiliation':
                self.row_data[label] = int(data.strip().strip('[]').strip())
            elif label == 'Index':
                self.row_data[label] = int(data.strip().strip('[]').strip())
            else:
                self.row_data[label] = data

def export_collaborator_emails():
    group_name = 'Collaborators'
    group_members = client.discourse_get_group_members(group_name=group_name)
    log.debug(f'Group member list obtained: {len(group_members)}')
    group_members_with_emails = []
    for member in group_members:
        member_with_emails = member
        member_emails = client.discourse_get_member_emails(username=member['username'])
        member_with_emails['emails'] = member_emails
        group_members_with_emails.append(member_with_emails)
    return group_members_with_emails


def compile_personnel_sources(muses_personnel_table_filepath=''):
    # group_members_with_emails = export_collaborator_emails()
    # # log.debug(yaml.dump(group_members_with_emails))
    # with open('collaboration_members.json', 'w') as outfile:
    #     json.dump(group_members_with_emails, outfile, indent=2)

    with open('collaboration_members.json', 'r') as infile:
        members = json.load(infile)

    member_registration_info = []
    with open('collaborator_registration_info.csv', newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='|')
        for record in csv_reader:
            member_registration_info.append({
                'email': record[1].strip(),
                'affiliation': record[3].strip(),
            })

    personnel_list = []
    with open(muses_personnel_table_filepath, newline='') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='|')
        for record in csv_reader:
            personnel_list.append({
                'email': record[2].strip(),
                'affiliation': record[4].strip(),
            })
    # log.debug(yaml.dump(personnel_list))
    # sys.exit()
    unaffiliated_collaborators = []
    collaborator_info_list = []
    for member in members:
        member_email = member['emails']['email']
        member_name = member['name']
        affiliation_list = [reg_info['affiliation'] for reg_info in member_registration_info if reg_info['email'] == member_email]
        affiliation_list += [personnel_info['affiliation'] for personnel_info in personnel_list if personnel_info['email'] == member_email]
        if affiliation_list:
            affiliation = affiliation_list[0]
            log.info(f'''Member "{member_name}" affiliation: "{affiliation}"''')
            # collaborator_info_list.append(f'''"{member_name}" ("{member_email}"): {affiliation}''')
            collaborator_info_list.append({
                'name': member_name,
                'email': member_email,
                'affiliation': affiliation,
            })
        else:
            log.warning(f'''    Cannot find affiliation for member "{member_name}" ("{member_email}")''')
            unaffiliated_collaborators.append(f'''"{member_name}" ("{member_email}")''')
    
    collaborator_info_list = sorted(collaborator_info_list, key=lambda collab: collab['affiliation'])
    log.debug(f'\nCollaborator information ({len(collaborator_info_list)}):\n')
    for collaborator_info in collaborator_info_list:
        log.debug(f'''* {collaborator_info['affiliation']}: {collaborator_info['name']}''')

    log.debug(f'\nCollaborators without affiliation info ({len(unaffiliated_collaborators)}):\n')
    for unregistered_collaborator in unaffiliated_collaborators:
        log.debug(f'''* {unregistered_collaborator}''')

def render_latex_authors(personnel_info=[], institutions=[], out_filepath='/tmp/authors.tex'):
    affiliations = []
    latex_str = ''
    ## For each row in the personnel table, extract the name and affiliation
    for row in personnel_info:
        try:
            assert row['Name']
            assert row['Affiliation']
            name = row['Name']
            affiliation = row['Affiliation']
        except Exception as e:
            log.error(f'''{e}: {json.dumps(row)}''')
            continue
        ## Compile list of affiliations with no duplicates
        if affiliation not in affiliations:
            affiliations.append(affiliation)
            # affiliation_idx = len(affiliations) - 1
        # else:
        #     for idx, aff in enumerate(affiliations):
        #         if aff == affiliation:
        #             # affiliation_idx = idx
        #             break
    #     latex_str += f'''\\author[aff{affiliation_idx}]{{{name}}}\n'''
    # latex_str += f'''\n'''
    # ## Enumerate affiliations
    # for idx, aff in enumerate(affiliations):
    #     latex_str += f'''\\affiliation[aff{idx}]{{{aff}}}\n'''

    latex_str = ''
    for idx, aff in enumerate(affiliations):
        affiliate_names = [person['Name'] for person in personnel_info if person['Affiliation'] == aff]
        affiliate_names = sorted(affiliate_names, key=lambda name: name.split()[-1])
        for name in affiliate_names:
            latex_str += f'''\\author{{{name}}}\n'''
        latex_str += f'''\\affiliation{{{aff}}}\n\n'''

    latex_str = ''
    members = sorted(personnel_info, key=lambda member: member['Name'].split()[-1])
    for member in members:
        latex_str += f'''\\author{{{member['Name']}}}\n'''

    ## Output to file
    with open(out_filepath, 'w') as outfile:
        outfile.write(latex_str)
    return latex_str

def import_personnel_table(out_filepath='/tmp/collaborator_info.json'):
    ## Download data from the forum topic using the Discourse API
    client = DiscourseApi(conf=config)
    post_data = client.discourse_get_topic(topic_id=41)
    ## Extract the message HTML containing the personnel information table
    post_html = post_data['post_stream']['posts'][0]['cooked']
    # log.info(post_html)
    # with open('collaborator_info.html', 'w') as html_source:
    #     html_source.write(post_html)

    parser = MyHTMLParser()
    parser.feed(post_html)
    # log.info(yaml.dump(parser.rows))
    log.info(f'''Number of personnel: {len(parser.personnel)}''')
    # with open('collaborator_info.yaml', 'w') as outfile:
    #     yaml.dump(parser.rows, outfile)
    personnel_info = parser.personnel
    institutions = parser.institutions
    log.debug(json.dumps(institutions))
    personnel_info_with_affiliations = []
    for person in personnel_info:
        person_with_aff = person
        person_with_aff['Affiliation'] = [aff['Institution'] for aff in institutions if aff['Index'] == person_with_aff['Affiliation']][0]
        personnel_info_with_affiliations.append(person_with_aff)
    with open(out_filepath, 'w') as outfile:
        json.dump(personnel_info, outfile, indent=2)
    return personnel_info_with_affiliations

if __name__ == "__main__":

    ## Download personnel table from forum
    personnel_info = import_personnel_table(out_filepath=os.path.join(config['outputBasePath'], 'collaborator_info.json'))

    # with open('collaborator_info.json', 'r') as infile:
    #     personnel_info = json.load(infile)

    ## Render author list with no affiliation redundancy
    latex_str = render_latex_authors(personnel_info=personnel_info, out_filepath=os.path.join(config['outputBasePath'], 'authors.tex'))
