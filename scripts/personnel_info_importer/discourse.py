import requests
import time
import logging

try:
    from global_vars import config
except:
    pass

# Configure logging
logging.basicConfig(format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
log = logging.getLogger(__name__)
try:
    log.setLevel(config['server']['logLevel'].upper())
except:
    log.setLevel('WARNING')

def add_user_to_group(email='', group_name=''):
    client = DiscourseApi()
    group_info = [group for group in client.discourse_get_groups()['groups'] if group['name'] == group_name][0]
    group_id = group_info['id']
    new_users = client.discourse_get_users()
    # log.debug(f'''{json.dumps(new_discourse_users, indent=2)}''')
    for new_user in new_users:
        new_user_info = client.discourse_get_user_info(new_user['id'])
        new_user_email = client.discourse_get_user_emails(new_user_info['username'])['email']
        if new_user_email == email:
            if [grp for grp in new_user_info['groups'] if grp['name'] == group_name]:
                log.debug(f'''Discourse user "{new_user_email}" already in group "{group_name}"''')
                return False
            else:
                log.debug(f'''Adding Discourse user "{new_user_email}" to group: {group_name}''')
                client.discourse_add_group_members([new_user['username']], group_id)
                return True
    raise Exception(f'''No Discourse user associated with email "{email}".''') 


class DiscourseApi():
    ## ref: https://docs.discourse.org/
    def __init__(self, conf=None):
        try:
            ## Hack added for consistency with duplicate `discourse.py` module used by the API server
            conf = config
        except:
            pass
        self.api_user = conf['discourse']['apiUser']
        self.api_key = conf['discourse']['apiKey']
        self.base_url = conf['discourse']['base_url']
        
    def discourse_get_users(self, flag='new'):
        r = requests.request( 'GET',
            f'''{self.base_url}/admin/users/list/{flag}.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        try:
            assert r.status_code == 200
            response = r.json()
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_get_users). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_users(flag)
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_user_info(self, user_id):
        r = requests.request( 'GET',
            f'''{self.base_url}/admin/users/{user_id}.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        try:
            assert r.status_code == 200
            response = r.json()
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_get_user_info). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_user_info(user_id)
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_user_emails(self, username):
        r = requests.request( 'GET',
            f'''{self.base_url}/users/{username}/emails.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        try:
            assert r.status_code == 200
            response = r.json()
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_get_user_emails). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_user_emails(username)
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_groups(self):
        r = requests.request( 'GET',
            f'''{self.base_url}/groups.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        try:
            assert r.status_code == 200
            response = r.json()
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_get_groups). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_groups()
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_add_group_members(self, usernames, group_id):
        username_list = ','.join(usernames)
        params = {
            'usernames': username_list,
        }
        r = requests.request( 'PUT',
            f'''{self.base_url}/groups/{group_id}/members.json''',
            json=params,
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        try:
            assert r.status_code == 200
            response = r.json()
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_add_group_members). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_add_group_members(usernames, group_id)
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
        return response

    def discourse_get_group_members(self, group_name):
        '''ref: https://docs.discourse.org/#operation/listGroupMembers'''
        r = requests.request( 'GET',
            f'''{self.base_url}/groups/{group_name}/members.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
            params={
                'limit': 999,
            }
        )
        try:
            assert r.status_code == 200
            ## Return only member list. Omit the `owner` and `meta` data
            response = r.json()['members']
        except:
            try:
                ## If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds'])+5
                        log.debug(f'''Rate limited (discourse_get_group_members). Waiting {wait_duration} seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_group_members(group_name)
            except:
                log.error(r.status_code)
                log.error(r.text)
                response = r
        return response

    def discourse_get_member_emails(self, username):
        '''ref: https://docs.discourse.org/#operation/getUserEmails'''
        log.debug(f'Executing "discourse_get_member_emails(username={username})"...')
        r = requests.request( 'GET',
            f'''{self.base_url}/u/{username}/emails.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+1
            except:
                ## Failed to get a precise wait time
                log.debug(f'''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_member_emails). Waiting {wait_duration} seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_member_emails(username)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_post(self, post_id):
        '''ref: https://docs.discourse.org/#tag/Posts/operation/getPost'''
        log.debug(f'Executing "discourse_get_post(post_id={post_id})"...')
        r = requests.request( 'GET',
            f'''{self.base_url}/posts/{post_id}.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            },
        )
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+1
            except:
                ## Failed to get a precise wait time
                log.debug(f'''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_post). Waiting {wait_duration} seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_post(post_id=post_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_topic_posts(self, topic_id: int = 0, post_id: int = 0):
        '''ref: https://docs.discourse.org/#tag/Topics/operation/getSpecificPostsFromTopic'''
        r = requests.request( 'GET',
            f'''{self.base_url}/t/{topic_id}/posts.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            }, 
            params={
                "post_ids[]": post_id
            }
        )
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+1
            except:
                ## Failed to get a precise wait time
                log.debug(f'''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_topic_posts). Waiting {wait_duration} seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_topic_posts(topic_id=topic_id, post_id=post_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_topic(self, topic_id: int = 0):
        '''ref: https://docs.discourse.org/#tag/Topics/operation/getTopic'''
        r = requests.request( 'GET',
            f'''{self.base_url}/t/{topic_id}.json''',
            headers={
                'Api-Username': self.api_user,
                'Api-Key': self.api_key,
            }, 
        )
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+1
            except:
                ## Failed to get a precise wait time
                log.debug(f'''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_topic). Waiting {wait_duration} seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_topic(topic_id=topic_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response
