#!/bin/bash

set -e

###################################################################################################
##
## This script assumes that you have SSH access to a worker node such as nsf-muses-worker-01.
## Use sshfs to mount a temporary folder to the worker node, and copy this script into that folder.
##
##     $ sshfs centos@nsf-muses-worker-01:/tmp/backup_check /tmp/backup_check
##
## Open a terminal on the worker node and run as root.
##
##     $ ssh nsf-muses-worker-01
##     [centos@nsf-muses-worker-01 ~]$ sudo -i
## 
## Mount the backups stored on Taiga.
## 
##     [root@nsf-muses-worker-01 ~]# mount -t nfs taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/bbdr/cluster-nsf-muses/backups /backups
## 
## Run the script.
##
##     [root@nsf-muses-worker-01 ~]# bash /tmp/backup_check/check_backups.sh 
##
###################################################################################################


dbpath_base="/tmp/backup_check"

repair_duplicati_db() {
    backup_path=$1
    dbpath=$2
    echo "${dbpath}"
    docker run --rm -it \
        -v /backups:/backups:ro \
        -v /tmp/backup_check:/tmp/backup_check \
        registry.gitlab.com/decentci/charts/data-backup-agent:0.7.1 \
        duplicati-cli \
        repair \
        "${backup_path}" \
        --no-encryption=true \
        --dbpath="${dbpath}"
}

list_snapshots() {
    backup_path=$1
    dbpath=$2
    docker run --rm -it \
        -v /backups:/backups:ro \
        -v /tmp/backup_check:/tmp/backup_check \
        registry.gitlab.com/decentci/charts/data-backup-agent:0.7.1 \
        duplicati-cli \
        find \
        "${backup_path}" \
        --no-encryption=true \
        --dbpath="${dbpath}"
}

list_files() {
    backup_path=$1
    dbpath=$2
    docker run --rm -it \
        -v /backups:/backups:ro \
        -v /tmp/backup_check:/tmp/backup_check \
        registry.gitlab.com/decentci/charts/data-backup-agent:0.7.1 \
        duplicati-cli \
        find \
        "${backup_path}" \
        "*" \
        --no-encryption=true \
        --dbpath="${dbpath}"
}

check_keycloak() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking keycloak database..."
    rsync -va "/backups/keycloak/duplicati_db/decentci-backups-keycloak-db.sqlite" "${dbpath_base}/keycloak-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/keycloak/decentci-backups-keycloak-db" "${dbpath_base}/keycloak-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/keycloak/decentci-backups-keycloak-db" "${dbpath_base}/keycloak-db.sqlite"
}

check_muses_api() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking muses-api database..."
    rsync -va "/backups/muses-api/duplicati_db/decentci-backups-muses-api-db.sqlite" "${dbpath_base}/muses-api-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/muses-api/decentci-backups-muses-api-db" "${dbpath_base}/muses-api-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/muses-api/decentci-backups-muses-api-db" "${dbpath_base}/muses-api-db.sqlite"
}

check_indico() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking indico database..."
    rsync -va "/backups/indico/duplicati_db/decentci-backups-indico-db.sqlite" "${dbpath_base}/indico-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/indico/decentci-backups-indico-db" "${dbpath_base}/indico-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/indico/decentci-backups-indico-db" "${dbpath_base}/indico-db.sqlite"

    echo "Checking indico files..."
    rsync -va "/backups/indico/duplicati_db/decentci-backups-indico-files.sqlite" "${dbpath_base}/indico-files.sqlite"
    rsync -a "/backups/indico/duplicati_db/decentci-backups-indico-files.sqlite" "${dbpath_base}/indico-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/indico/decentci-backups-indico-files" "${dbpath_base}/indico-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/indico/decentci-backups-indico-files" "${dbpath_base}/indico-files.sqlite"
}

check_synapse() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking synapse database..."
    rsync -va "/backups/matrix-system/duplicati_db/decentci-backups-synapse-db.sqlite" "${dbpath_base}/synapse-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/matrix-system/decentci-backups-synapse-db" "${dbpath_base}/synapse-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/matrix-system/decentci-backups-synapse-db" "${dbpath_base}/synapse-db.sqlite"

    echo "Checking synapse files..."
    rsync -va "/backups/matrix-system/duplicati_db/decentci-backups-synapse-files.sqlite" "${dbpath_base}/synapse-files.sqlite"
    rsync -a "/backups/matrix-system/duplicati_db/decentci-backups-synapse-files.sqlite" "${dbpath_base}/synapse-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/matrix-system/decentci-backups-synapse-files" "${dbpath_base}/synapse-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/matrix-system/decentci-backups-synapse-files" "${dbpath_base}/synapse-files.sqlite"
}

check_hedgedoc() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking hedgedoc database..."
    rsync -va "/backups/hedgedoc/duplicati_db/decentci-backups-hedgedoc-db.sqlite" "${dbpath_base}/hedgedoc-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/hedgedoc/decentci-backups-hedgedoc-db" "${dbpath_base}/hedgedoc-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/hedgedoc/decentci-backups-hedgedoc-db" "${dbpath_base}/hedgedoc-db.sqlite"

    echo "Checking hedgedoc files..."
    rsync -va "/backups/hedgedoc/duplicati_db/decentci-backups-hedgedoc-files.sqlite" "${dbpath_base}/hedgedoc-files.sqlite"
    rsync -a "/backups/hedgedoc/duplicati_db/decentci-backups-hedgedoc-files.sqlite" "${dbpath_base}/hedgedoc-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/hedgedoc/decentci-backups-hedgedoc-files" "${dbpath_base}/hedgedoc-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/hedgedoc/decentci-backups-hedgedoc-files" "${dbpath_base}/hedgedoc-files.sqlite"
}

check_uptime_kuma() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking uptime-kuma files..."
    rsync -va "/backups/uptime-kuma/duplicati_db/decentci-backups-uptime-kuma-files.sqlite" "${dbpath_base}/uptime-kuma-files.sqlite"
    rsync -a "/backups/uptime-kuma/duplicati_db/decentci-backups-uptime-kuma-files.sqlite" "${dbpath_base}/uptime-kuma-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/uptime-kuma/decentci-backups-uptime-kuma-files" "${dbpath_base}/uptime-kuma-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/uptime-kuma/decentci-backups-uptime-kuma-files" "${dbpath_base}/uptime-kuma-files.sqlite"
}

check_wordpress() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking wordpress database..."
    rsync -va "/backups/wordpress/duplicati_db/decentci-backups-wordpress-db.sqlite" "${dbpath_base}/wordpress-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/wordpress/decentci-backups-wordpress-db" "${dbpath_base}/wordpress-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/wordpress/decentci-backups-wordpress-db" "${dbpath_base}/wordpress-db.sqlite"

    echo "Checking wordpress files..."
    rsync -va "/backups/wordpress/duplicati_db/decentci-backups-wordpress-files.sqlite" "${dbpath_base}/wordpress-files.sqlite"
    rsync -a "/backups/wordpress/duplicati_db/decentci-backups-wordpress-files.sqlite" "${dbpath_base}/wordpress-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/wordpress/decentci-backups-wordpress-files" "${dbpath_base}/wordpress-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/wordpress/decentci-backups-wordpress-files" "${dbpath_base}/wordpress-files.sqlite"
}

check_nextcloud() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking Nextcloud database..."
    rsync -va "/backups/nextcloud/duplicati_db/decentci-backups-nextcloud-db.sqlite" "${dbpath_base}/nextcloud-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/nextcloud/decentci-backups-nextcloud-db" "${dbpath_base}/nextcloud-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/nextcloud/decentci-backups-nextcloud-db" "${dbpath_base}/nextcloud-db.sqlite"

    echo "Checking Nextcloud files..."
    rsync -va "/backups/nextcloud/duplicati_db/decentci-backups-nextcloud-files.sqlite" "${dbpath_base}/nextcloud-files.sqlite"
    rsync -a "/backups/nextcloud/duplicati_db/decentci-backups-nextcloud-files.sqlite" "${dbpath_base}/nextcloud-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/nextcloud/decentci-backups-nextcloud-files" "${dbpath_base}/nextcloud-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/nextcloud/decentci-backups-nextcloud-files" "${dbpath_base}/nextcloud-files.sqlite"
}

check_discourse() {
    echo -e "\n\n\n##################################################################################"
    echo "Checking Discourse database..."
    rsync -va "/backups/discourse/duplicati_db/decentci-backups-discourse-db.sqlite" "${dbpath_base}/discourse-db.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/discourse/decentci-backups-discourse-db" "${dbpath_base}/discourse-db.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/discourse/decentci-backups-discourse-db" "${dbpath_base}/discourse-db.sqlite"

    echo "Checking Discourse files..."
    rsync -va "/backups/discourse/duplicati_db/decentci-backups-discourse-files.sqlite" "${dbpath_base}/discourse-files.sqlite"
    rsync -a "/backups/discourse/duplicati_db/decentci-backups-discourse-files.sqlite" "${dbpath_base}/discourse-files.sqlite"
    echo "Snapshots..."
    list_snapshots "/backups/discourse/decentci-backups-discourse-files" "${dbpath_base}/discourse-files.sqlite"
    echo "Files in latest snapshot..."
    list_files "/backups/discourse/decentci-backups-discourse-files" "${dbpath_base}/discourse-files.sqlite"
}

echo "Mounting Taiga backups to \"/backups\" ..."
mount -t nfs taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/bbdr/cluster-nsf-muses/backups /backups || /bin/true
echo "\"/backups\" mounted."

## Check individual application backups
##
# check_nextcloud
# check_discourse
# check_hedgedoc
# check_indico
# check_keycloak
# check_muses_api
# check_uptime_kuma
# check_wordpress
# check_synapse

exit 0
