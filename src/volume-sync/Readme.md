Debugger deployment
==================================

Build and deploy a debugger deployment for in-cluster diagnostics.

```bash
docker build -t registry.gitlab.com/nsf-muses/deployment/kubernetes/debugger:latest .
docker push registry.gitlab.com/nsf-muses/deployment/kubernetes/debugger:latest
```
