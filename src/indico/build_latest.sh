#!/bin/sh
# Builds the latest production Indico image

if ! command -v jq >/dev/null 2>&1
then
    echo 'jq is not installed'
    exit
fi

LATEST_VERSION=$(curl --insecure https://api.github.com/repos/indico/indico/releases/latest | jq '.name[1:]' --raw-output)
echo "Building Indico ${LATEST_VERSION}..."
docker build worker -t registry.gitlab.com/nsf-muses/deployment/kubernetes/indico-worker:$LATEST_VERSION --build-arg tag=${LATEST_VERSION}
docker build nginx  -t registry.gitlab.com/nsf-muses/deployment/kubernetes/indico-nginx:$LATEST_VERSION  --build-arg tag=${LATEST_VERSION}
