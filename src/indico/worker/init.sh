#!/bin/bash

# Populate Indico config template with environment variable values
envsubst < "${INDICO_CONFIG}" > /tmp/indico.conf
cp /tmp/indico.conf "${INDICO_CONFIG}"

# Hack to address logout bug. See
#   * https://talk.getindico.io/t/single-sign-out-with-keycloak-18-0-0/2696/1
#   * https://github.com/keycloak/keycloak/issues/14010
#
sed -i "s/{'post_logout_redirect_uri': return_url}/{}/g" /opt/indico/.venv/lib/python3.10/site-packages/flask_multipass/providers/authlib.py
