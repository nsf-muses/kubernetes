#!/bin/bash

# Clear any previous Docker run directory
rm -rf $HOME/.docker/run/
mkdir -p $HOME/.docker/run/

# Launch the rootless Docker daemon
dockerd-rootless.sh &

# Execute the input command
args=("$@")
# exec ${args[@]}
bash -c "${args[@]}"
