#!/usr/bin/env python3

import os
import sys
import requests
from datetime import datetime
import time
import json
import yaml
import hvac

'''
First export Vault URL and access token:

    export VAULT_URL="https://vault.musesframework.io"
    export VAULT_TOKEN="s.r00t/t0k3n"

Backup secrets:

    python vault_backup_and_restore.py backup vault_backup.json

Import secrets:

    python vault_backup_and_restore.py import vault_backup.json

'''

client = hvac.Client(
    url=os.environ['VAULT_URL'],
    token=os.environ['VAULT_TOKEN'],
)

def list_keys(secret_path, secrets={}, mount_point='kv'):
    list_response = client.secrets.kv.v2.list_secrets(
        mount_point=mount_point,
        path=secret_path,
    )
    for key in list_response['data']['keys']:
        new_path = f'''{secret_path}{key}'''
        if key.endswith('/'):
            secrets = list_keys(f'''{new_path}''', secrets=secrets, mount_point=mount_point)
        else:
            full_path = f'''{mount_point}/{new_path}'''
            print(full_path)
            secret_version_response = client.secrets.kv.v2.read_secret_version(
                mount_point=mount_point,
                path=new_path,
            )
            # print(f'''{json.dumps(secret_version_response['data']['data'])}''')
            secrets.append({
                'path': new_path,
                'data': secret_version_response['data']['data'],
            })
    return secrets

if __name__ == '__main__':
    
    assert len(sys.argv) > 2
    filepath = sys.argv[2]
    mount_point = 'kv'
    if len(sys.argv) > 3:
        mount_point = sys.argv[3]
    #
    # Backup
    #
    if sys.argv[1] == 'backup':
        # print(client.is_authenticated())
        print('List all secrets:\n')
        root_secret_path = ''
        secrets = list_keys(root_secret_path, secrets=[], mount_point=mount_point)

        # print(json.dumps(secrets, indent=2))
        backup_data = {
            'backup_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
            'vault_url': os.environ['VAULT_URL'],
            'mount_point': mount_point,
            'secrets': secrets,
        }
        with open(filepath, 'w') as secrets_file:
            json.dump(backup_data, secrets_file, indent=2)
    #
    # Import
    #
    elif sys.argv[1] == 'import':
        with open(filepath, 'r') as secrets_file:
            data = json.load(secrets_file)
        # # Ensure that the backup matches the vault URL
        # assert data['vault_url'] == os.environ['VAULT_URL']
        for secret in data['secrets']:
            try:
                client.secrets.kv.v2.create_or_update_secret(
                    path=secret['path'],
                    secret=secret['data'],
                    mount_point=data['mount_point'],
                )
            except Exception as e:
                print(f'''Error creating secret: "{secret}"''')
