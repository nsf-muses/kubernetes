
var validateEmailAddress = function(emailStr) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(emailStr).toLowerCase());
}

var showFormError = function(message) {
    // Show error message
    $("#error").html(message);
    $("#error").show();
    // Re-enable submit button
    $("#btnSubmit").prop("disabled", false);
}

var validateCollaboratorInfo = function(data) {
    window.console.log(`data: ${JSON.stringify(data)}`);
    var collaboratorInfo = data.collaboratorInfo;
    if (collaboratorInfo.workingGroups.length < 1) {
        showFormError('You must select at least one working group.')
        return false;
    }
    if (!collaboratorInfo.conduct) {
        showFormError('You must agree to the Code of Conduct.')
        return false;
    }
    if (collaboratorInfo.motivation.length < 50) {
        showFormError('Please provide more detail about your motivation.')
        return false;
    }
    if (collaboratorInfo.motivation.length > 1000) {
        showFormError('Please limit your motivation response to 1000 characters.')
        return false;
    }
    return true;
}

var validatePersonalInfo = function(data) {
    // window.console.log(`data: ${JSON.stringify(data)}`);
    var personalInfo = data.personalInfo;
    var email = data.email;
    if (!validateEmailAddress(email)) {
        showFormError('You must enter a valid email address.')
        return false;
    }
    if (personalInfo.affiliation === '') {
        showFormError('You must enter an affiliated institution or organization.')
        return false;
    }
    if (personalInfo.country === '0') {
        showFormError('You must select the host country of your affiliation.')
        return false;
    }
    if (personalInfo.role === '0') {
        showFormError('Please select your role.')
        return false;
    }
    if (personalInfo.research === '0') {
        showFormError('Please select your research field.')
        return false;
    }
    var selected = [];
    var list = document.getElementById('race')
    for (i = 0; i < list.options.length; i++) {
        if (list.options[ i ].selected) {
            selected.push(list.options[ i ].value);
        }
    }
    if (selected.length < 1) {
        showFormError('Please select your race(s) or "Decline to answer".')
        return false;
    }
    if (personalInfo.ethnicity === '0') {
        showFormError('Please select your ethnicity or "Decline to answer".')
        return false;
    }
    if (personalInfo.gender === '0') {
        showFormError('Please select your gender or "Decline to answer".')
        return false;
    }
    return true;
}

$(document).ready(function () {
 
    $("#btnSubmit").click(function (event) {
 
        //stop submit the form, we will post it manually.
        event.preventDefault();
        
        // disabled the submit button
        $("#btnSubmit").prop("disabled", true);
        // hide the previous error message if visible
        $("#error").hide();
        
        
        // Package JSON payload
        var email = $('#email').val();
        var personalInfo = {
            affiliation: $('#affiliation').val(),
            country: $('#country').val(),
            research: $('#research').val(),
            role: $('#role').val(),
            gender: $('#gender').val(),
            race: $('#race').val(),
            ethnicity: $('#ethnicity').val()
        }
        data = {
            email: email,
            personalInfo: personalInfo
        }
        
        //// Validate form inputs
        //
        if (!validatePersonalInfo(data)) {
            return;
        }
        
        {% if which_form == 'collaborator' -%}
        var form_data = new FormData(document.querySelector("#register-form"));
        // window.console.log(`motiv: ${$('#motivation').val()}`);
        data.collaboratorInfo = {
            workingGroups: form_data.getAll("workgroups[]"),
            motivation: $('#motivation').val(),
            conduct: $('#conduct').is(":checked")
        }
        if (!validateCollaboratorInfo(data)) {
            return;
        }
        {% endif -%}
        

        $("#spinner").show();
        
        // Submit form to API server for processing
        $.ajax({
            type: 'POST',
            url: '/{{base_path}}/register/{{which_form}}',
            data: JSON.stringify (data),
            contentType: "application/json",
            dataType: 'json',
            timeout: 0,
            success: function (data) {
                // Hide submit button and show success message
                $("#btnSubmit").hide();
                $("#result").show();
                $("#spinner").hide();
                // console.log("SUCCESS : ", data['msg']);
                // $("#btnSubmit").prop("disabled", false);
                // window.location.href = 'https://forum.musesframework.io';
            },
            error: function (e) {
                // Display server error response
                $("#result").hide();
                $("#spinner").hide();
                $("#error").html(`Server error response: ${e.responseText}`);
                $("#error").show();
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);
            }
        });
 
    });
 
});
