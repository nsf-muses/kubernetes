jQuery.fn.selectText = function(){
  var doc = document;
  var element = this[0];
  console.log(this, element);
  if (doc.body.createTextRange) {
      var range = document.body.createTextRange();
      range.moveToElementText(element);
      range.select();
  } else if (window.getSelection) {
      var selection = window.getSelection();        
      var range = document.createRange();
      range.selectNodeContents(element);
      selection.removeAllRanges();
      selection.addRange(range);
  }
};
$('#token').selectText();
  // btnCopy = document.getElementById( 'copy' ),
  // paste   = document.getElementById( 'empty-field' );

// btnCopy.addEventListener( 'click', function(){
//   toCopy.select();
//   paste.value = '';
  
//   if ( document.execCommand( 'copy' ) ) {
//       btnCopy.classList.add( 'copied' );
//     paste.focus();
    
//       var temp = setInterval( function(){
//         btnCopy.classList.remove( 'copied' );
//         clearInterval(temp);
//       }, 600 );
    
//   } else {
//     console.info( 'document.execCommand went wrong…' )
//   }
  
//   return false;
// } );
