var showEditForm = function(attribute) {
  $(".input-container").hide();
  $(`#${attribute}-container`).show();
  
}

var updateAttribute = function(operation, attribute, value, scalar=false) {

  data = {
    'attribute': attribute,
    'value': value,
    'scalar': scalar
  }
  let requestType = 'GET'
  switch (operation) {
    case 'add':
      case 'update':
        requestType = 'POST'
        break;
        case 'delete':
          if (!confirm(`Delete ${attribute}?`)) {
            return;
          }
          requestType = 'DELETE'
          break;
          default:
            return;
          }
    $(".input-container").hide();
    $("#error").hide();
    $("#spinner").show();
  // Submit form to API server for processing
  $.ajax({
      type: requestType,
      url: '{{ profile_url }}',
      data: JSON.stringify(data),
      contentType: "application/json",
      dataType: 'json',
      timeout: 0,
      success: function (data) {
        $("#result").show().delay(1000).fadeOut(400,"swing", () => {
          window.location.href = '{{ profile_url }}';
        });
        $("#spinner").hide();
        if ( data !== '' ){
          console.log(`${data}`)
        }
      },
      error: function (e) {
        $("#result").hide();
        $("#spinner").hide();
        $("#error").html(`Server error response: ${e.responseText}`);
        $("#error").show();
      }
  });
}

$(document).ready(function () {

  $("#newAffiliationButton").click(function (event) {
      event.preventDefault()
      if ( $('#newAffiliation').val() === '' ) {
        return;
      }
      updateAttribute('add', 'affiliation', $('#newAffiliation').val())
      $('#newAffiliation').val('')
  });

  $("#newWorkingGroupButton").click(function (event) {
      event.preventDefault()
      let value = $('#newWorkingGroup').find(":selected").val()
      console.log(value)
      if ( value === '' ) {
        return;
      }
      updateAttribute('add', 'working_group', value)
  });

  $("#newRoleButton").click(function (event) {
      event.preventDefault()
      let value = $('#newRole').find(":selected").val()
      console.log(value)
      if ( value === '' ) {
        return;
      }
      updateAttribute('add', 'role', value, scalar=true)
  });

  $("#updateAuthorNameButton").click(function (event) {
      event.preventDefault()
      if ( $('#updateAuthorName').val() === '' ) {
        return;
      }
      updateAttribute('update', 'author_name', $('#updateAuthorName').val(), scalar=true)
      $('#updateAuthorName').val('')
  });

  $("#updateAcknowledgementsButton").click(function (event) {
      event.preventDefault()
      if ( $('#updateAcknowledgements').val() === '' ) {
        return;
      }
      updateAttribute('update', 'acknowledgements', $('#updateAcknowledgements').val(), scalar=true)
      $('#updateAcknowledgements').val('')
  });

  $("#updateWebsiteButton").click(function (event) {
      event.preventDefault()
      if ( $('#updateWebsite').val() === '' ) {
        return;
      }
      updateAttribute('update', 'website', $('#updateWebsite').val(), scalar=true)
      $('#updateWebsite').val('')
  });
});
