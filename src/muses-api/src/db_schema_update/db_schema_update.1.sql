CREATE TABLE IF NOT EXISTS `demographics`(
	`id` int NOT NULL AUTO_INCREMENT,
	`time` datetime DEFAULT 0,
	`token` varchar(500) DEFAULT '',
	`source` varchar(32) NOT NULL,
	`race` varchar(500) NOT NULL,
	`ethnicity` varchar(500) NOT NULL,
	`gender` varchar(500) NOT NULL,
	PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)
)
#---
CREATE TABLE IF NOT EXISTS `user_info`(
	`id` int NOT NULL AUTO_INCREMENT,
	`keycloak_id` varchar(500) NOT NULL,
	`email` varchar(500) NOT NULL,
	`country` varchar(500) NOT NULL DEFAULT '',
	`affiliation` varchar(500) NOT NULL DEFAULT '',
	`research` varchar(500) NOT NULL DEFAULT '',
	`role` varchar(500) NOT NULL DEFAULT '',
	`working_groups` varchar(500) NOT NULL DEFAULT '',
	`motivation` varchar(2000) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)
)
#---
CREATE TABLE IF NOT EXISTS `meta`(
	`Lock` char(1) NOT NULL DEFAULT 'X',
	`schema_version` int NOT NULL DEFAULT 0,
	constraint PK_meta PRIMARY KEY (`Lock`),
	constraint CK_meta_Locked CHECK (`Lock`='X')
)
