import os
import global_vars
from global_vars import STATUS_OK, config, log
import tornado
import tornado.ioloop
import tornado.web
import tornado.template
import json
from datetime import datetime
from email_utils import send_email
import jinja2
import keycloak
import discourse
import bcrypt
from urllib import parse
import time
from dbconnector import DbConnector
import re
from mimetypes import guess_type
from jwtutils import authenticated, encode_info, refresh_token, allowed_roles
from tornado.auth import OAuth2Mixin
import urllib.parse
from tornado import escape
from typing import Any, Dict
from discourse import DiscourseApi

# Get global instance of the job handler database interface
db = DbConnector(
    mysql_host=config['db']['host'],
    mysql_user=config['db']['user'],
    mysql_password=config['db']['pass'],
    mysql_database=config['db']['database'],
)

# Load Kubernetes API
try:
    import kubejob
except Exception as e:
    log.warning(f'''Failure loading Kubernetes client: {e}''')


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers",
                        "Origin, X-Requested-With, Content-Type, Accept, Authorization")
        self.set_header("Access-Control-Allow-Methods",
                        " POST, PUT, DELETE, OPTIONS, GET")

    def options(self):
        self.set_status(204)
        self.finish()

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def getarg(self, arg, default=None):
        '''
        Calls to this function in BaseHandler.get(), BaseHandler.post(), etc must be surrounded by try/except blocks like so:

            try:
                ownerId = self.getarg('ownerId')
            except:
                self.finish()
                return

        '''
        value = default
        try:
            # If the request encodes arguments in JSON, parse the body accordingly
            if 'Content-Type' in self.request.headers and self.request.headers['Content-Type'] in ['application/json', 'application/javascript']:
                data = tornado.escape.json_decode(self.request.body)
                if default == None:
                    # The argument is required and thus this will raise an exception if absent
                    value = data[arg]
                else:
                    # Set the value to the default
                    value = default if arg not in data else data[arg]
            # Otherwise assume the arguments are in the default content type
            else:
                # The argument is required and thus this will raise an exception if absent
                if default == None:
                    value = self.get_argument(arg)
                else:
                    value = self.get_argument(arg, default)
        except Exception as e:
            response = str(e).strip()
            log.error(response)
            # 400 Bad Request: The server could not understand the request due to invalid syntax.
            # The assumption is that if a function uses `getarg()` to get a required parameter,
            # then the request must be a bad request if this exception occurs.
            self.send_response(response, http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            raise e
        return value

    # The datetime type is not JSON serializable, so convert to string
    def json_converter(self, o):
        if isinstance(o, datetime):
            return o.__str__()

    def send_response(self, data='', http_status_code=global_vars.HTTP_OK, return_json=True, indent=None):
        if return_json:
            if indent:
                self.write(json.dumps(data, indent=indent, default=self.json_converter))
            else:
                self.write(json.dumps(data, default=self.json_converter))
            self.set_header('Content-Type', 'application/json')
        else:
            self.write(data)
        self.set_status(http_status_code)

def renderJinjaTemplate(tpl_filename, values={}):
    text = ''
    path, filename = os.path.split(os.path.join(os.path.dirname(__file__), 'templates', tpl_filename))
    text = jinja2.Environment(loader=jinja2.FileSystemLoader(path or './')).get_template(filename).render(values)
    return text

def escape_html(htmlstring):
    escapes = {'\"': '&quot;',
               '\'': '&#39;',
               '<': '&lt;',
               '>': '&gt;'}
    # This is done first to prevent escaping other escapes.
    htmlstring = htmlstring.replace('&', '&amp;')
    for seq, esc in escapes.items():
        htmlstring = htmlstring.replace(seq, esc)
    return htmlstring

def get_api_base_path():
    ## Generate API base path for links
    basePath = f'''{config['server']['basePath']}/{config['server']['apiBasePath']}'''
    basePath = basePath.strip('/')
    return basePath


def valid_job_id(job_id):
    # For testing purposes, treat the string 'invalid_job_id' as an invalid job_id
    return isinstance(job_id, str) and len(job_id) > 0


def construct_job_object(job_info):
    job = {}
    try:
        creationTime = job_info['creation_time']
        startTime = job_info['status']['start_time']
        endTime = job_info['status']['completion_time']
        destructionTime = None # TODO: Should we track deletion time?
        try:
            executionDuration = (endTime - startTime).total_seconds()
        except:
            executionDuration = None
        try:
            message = job_info['message']
        except:
            message = ''
        # Determine job phase. For definitions see:
        #     https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#ExecutionPhase
        job_phase = 'unknown'
        if creationTime:
            job_phase = 'pending'
        if startTime:
            job_phase = 'queued'
            if job_info['status']['active']:
                job_phase = 'executing'
            if job_info['status']['failed']:
                job_phase = 'error'
        if endTime:
            job_phase = 'completed'
            if not job_info['status']['succeeded'] or job_info['status']['failed']:
                job_phase = 'error'
        
        results = []
        try:
            for idx, filepath in enumerate(job_info['output_files']):
                results.append({
                    'id': idx,
                    'uri': filepath,
                    # 'mime-type': 'image/fits',
                    # 'size': '3000960',
                })
        except Exception as e:
            log.error(str(e))
            results = []
        # See job_schema.xml
        #   https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#jobobj
        job = {
            'jobId': job_info['job_id'],
            'runId': job_info['run_id'],
            'ownerId': '', # TODO: Track identity of job owner
            'phase': job_phase,
            'creationTime': creationTime,
            'startTime': startTime,
            'endTime': endTime,
            'executionDuration': executionDuration,
            'destruction': destructionTime,
            'parameters': {
                'command': job_info['command'],
                'environment': job_info['environment'],
            },
            'results': results,
            'errorSummary': {
                'message': message,
            },
            'jobInfo': {
            },
        }
    except Exception as e:
        log.error(str(e))
    return job


class RegisterCollaboratorFormHandler(BaseHandler):
    def get(self):
        ## Render webpage HTML
        html = renderJinjaTemplate('page_join_form.tpl.html', {
            'css': renderJinjaTemplate('page_join_form.tpl.css'),
            'javascript': renderJinjaTemplate('registration_form.tpl.js', {
                'which_form': 'collaborator',
                'base_path': get_api_base_path(),
            }),
            'personal_info_fields': renderJinjaTemplate('personal_info_fields.tpl.html'),
        })
        self.send_response(html, return_json=False)
        self.finish()
        return


class RegisterSeminarFormHandler(BaseHandler):
    def get(self):
        ## Render webpage HTML
        html = renderJinjaTemplate('page_seminar_form.tpl.html', {
            'css': renderJinjaTemplate('page_join_form.tpl.css'),
            'javascript': renderJinjaTemplate('registration_form.tpl.js', {
                'which_form': 'seminar',
                'base_path': get_api_base_path(),
            }),
            'personal_info_fields': renderJinjaTemplate('personal_info_fields.tpl.html'),
        })
        self.send_response(html, return_json=False)
        self.finish()
        return


class RegisterSeminarHandler(BaseHandler):
    def post(self):
        if config['devMode']:
            self.send_response(f'''Dev mode active. Taking no action.''', return_json=False)
            self.finish()
            return
        try:
            email = self.getarg('email').lower() # required
            personal_info = self.getarg('personalInfo') # required
        except:
            self.finish()
            return
        try:
            log.debug(f'''New seminar registration: "{email}"''')
            keycloak_client = keycloak.KeycloakApi()
            keycloak_user = keycloak_client.get_user_info(email=email)
            if not keycloak_user:
                self.send_response({'msg': f'''No MUSES account with email "{email}" found.'''}, http_status_code=global_vars.HTTP_NOT_FOUND, return_json=True)
                self.finish()
                return
            keycloak_user = keycloak_user[0]
            log.debug(f'''Keycloak user info: "{keycloak_user}"''')
            try:
                ## Generate hash of email address using secret salt
                email_hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(config['server']['hash_salt'], "utf-8"))
                ## Record demographic information in database
                if not db.insert_demographic_info(personal_info, token=email_hash):
                    log.error(f'''Error adding info to database: {json.dumps(personal_info)}''')
                ## Record other user information in database
                user_info = {
                    'email': email,
                    'keycloak_id': keycloak_user['id'],
                    'country': personal_info['country'],
                    'affiliation': personal_info['affiliation'],
                    'role': personal_info['role'],
                    'research': personal_info['research'],
                }
                if not db.insert_user_info(user_info):
                    log.error(f'''Error adding user info to database: {json.dumps(user_info)}''')
            except Exception as e:
                log.error(f'''Error adding info to database: {e}''')
            try:
                ## Add user to Discourse Collaborator group if not already a member
                added_to_group = discourse.add_user_to_group(email=email, group_name='seminar')
                if not added_to_group:
                    self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already registered for the seminar.''', http_status_code=global_vars.HTTP_OK, return_json=False)
                    self.finish()
                    return
            except Exception as e:
                log.error(f'''Error adding user to Discourse group: {e}''')
                self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" to Discourse group. {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                self.finish()
                return
            ## Render email message
            message_body = renderJinjaTemplate('email_seminar_register.tpl.html', {
                'email': email,
                'given_name': keycloak_user['firstName'],
                'family_name': keycloak_user['lastName'],
                'country': escape_html(personal_info['country']),
                'affiliation': escape_html(personal_info['affiliation']),
                'role': escape_html(personal_info['role']),
                'research': escape_html(personal_info['research']),
            })
            ## Inform MUSES leadership of the seminar registration
            send_email(
                recipients=config['email']['recipients'],
                email_subject='MUSES seminar series registration', 
                message_body=message_body,
                template_file='email_frame.tpl.html'
            )
        except Exception as e:
            log.error(f'''New seminar registration failed: {e}''')
            self.send_response({'msg': str(e)}, http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=True)
            self.finish()
            return
        self.send_response()
        self.finish()
        return


class RegisterCalculationEngineUserHandler(BaseHandler):
    def get(self, decision='', email_urlencoded=''):
        if config['devMode']:
            self.send_response('''Dev mode active. Taking no action.''', return_json=False)
            self.finish()
            return
        try:
            token_urlencoded = self.getarg('token')  # required
        except Exception:
            self.finish()
            return

        try:
            email = parse.unquote(email_urlencoded)
            token = parse.unquote(token_urlencoded)
            log.debug(f'''Approving Calculation Engine access request: "{email}"''')
            # Obtain Keycloak user info
            keycloak_client = keycloak.KeycloakApi()
            keycloak_user = keycloak_client.get_user_info(email=email)
            if not keycloak_user:
                self.send_response(f'''No MUSES account with email "{email}" found.''',
                                   http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            keycloak_user = keycloak_user[0]
            log.debug(f'''Keycloak user info: "{keycloak_user}"''')
            log.debug(f'''Token: "{token}"''')
            # Compare the auth token to the hash
            hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(config['server']['hash_salt'], "utf-8")).decode("utf-8")
            log.debug(f'''Hash: "{hash}"''')
            if token != hash:
                self.send_response('''Invalid authorization token.''',
                                   http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
                self.finish()
                return
            # Approve or Reject
            decision = decision.lower()
            if decision == 'approve':
                log.debug(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been approved.''')
                # Add user to Keycloak calculation engine access group if they are not already
                user_groups = keycloak_client.get_user_group_membership(keycloak_user['id'])
                log.debug(f'''user_groups: "{json.dumps(user_groups, indent=2)}"''')
                if [group for group in user_groups if group['path'] == '/ce']:
                    self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is '''
                                       '''already in the ce group.''', return_json=False)
                    self.finish()
                    return
                success = keycloak_client.add_user_to_group(keycloak_user['id'], group_name='ce')
                try:
                    # Add user to Discourse ce-users group if not already a member so that they can access the
                    # support category on the forum
                    if not discourse.add_user_to_group(email=email, group_name='ce-users'):
                        log.warning(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already '''
                                    '''a member of the "ce-users" Discourse group.''')
                except Exception as e:
                    log.error(f'''Error adding user to Discourse group: {e}''')
                    self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" '''
                                       '''to Discourse group "ce-users".''',
                                       http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return

                if not success:
                    self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" '''
                                       '''to ce group.''',
                                       http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
                # Render email message
                message_body = renderJinjaTemplate('email_body_approved_ce.tpl.html', {})
                # Send email notifying about approved acces
                send_email(
                    recipients=email,
                    email_subject='MUSES Calculation Engine access approved',
                    message_body=message_body,
                    template_file='email_frame.tpl.html'
                )
                self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has '''
                                   '''been notified of their acceptance.''', return_json=False)
                self.finish()
            elif decision == 'reject':
                log.debug(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" '''
                          '''has been rejected from the ce group.''')
                self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has '''
                                   '''NOT been notified of their rejection.''', return_json=False)
                self.finish()
                return
            else:
                self.send_response('''Invalid decision. Supported values are "accept" and "reject".''',
                                   http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
        except Exception as e:
            log.error(f'''CE access group approval failed: {e}''')
            self.send_response(str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return


class RegisterCollaboratorHandler(BaseHandler):
    def get(self, decision='', email_urlencoded=''):
        if config['devMode']:
            self.send_response(f'''Dev mode active. Taking no action.''', return_json=False)
            self.finish()
            return
        try:
            token_urlencoded = self.getarg('token')  # required
        except:
            self.finish()
            return
        try:
            email = parse.unquote(email_urlencoded)
            token = parse.unquote(token_urlencoded)
            log.debug(f'''Approving collaborator request: "{email}"''')
            ## Obtain Keycloak user info
            keycloak_client = keycloak.KeycloakApi()
            keycloak_user = keycloak_client.get_user_info(email=email)
            if not keycloak_user:
                self.send_response(f'''No MUSES account with email "{email}" found.''', http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            keycloak_user = keycloak_user[0]
            log.debug(f'''Keycloak user info: "{keycloak_user}"''')
            log.debug(f'''Token: "{token}"''')
            ## Compare the auth token to the hash
            hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(config['server']['hash_salt'], "utf-8")).decode("utf-8")
            log.debug(f'''Hash: "{hash}"''')
            if token != hash:
                self.send_response(f'''Invalid authorization token.''', http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
                self.finish()
                return
            ## Approve or Reject
            decision = decision.lower()
            if decision == 'approve':
                log.debug(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been approved.''')
                ## Add user to Keycloak Collaborator role if they are not already
                role_mapping = keycloak_client.get_user_role_mapping(keycloak_user['id'])
                log.debug(f'''role_mapping: "{json.dumps(role_mapping, indent=2)}"''')
                if [mapping for mapping in role_mapping['realmMappings'] if mapping['name'] == 'Collaborators']:
                    self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already in the Collaborator Keycloak realm role.''', return_json=False)
                    self.finish()
                    return
                success = keycloak_client.apply_role_mapping_to_user(keycloak_user['id'], role_id='654fc8c4-01c8-4525-a8fe-b717555c048f', role_name='Collaborators')
                if not success:
                    self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" to Collaborator role.''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
                try:
                    ## Add user to Discourse Collaborator group if not already a member
                    if not discourse.add_user_to_group(email=email, group_name='collaborators'):
                        log.warning(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already a member of the "collaborators" Discourse group.''')
                    if not discourse.add_user_to_group(email=email, group_name='seminar'):
                        log.warning(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" is already a member of the "seminar" Discourse group.''')
                except Exception as e:
                    log.error(f'''Error adding user to Discourse group: {e}''')
                    self.send_response(f'''Error adding "{keycloak_user['firstName']} {keycloak_user['lastName']}" to Discourse group.''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
                ## Render email message
                message_body = renderJinjaTemplate('email_body_approved.tpl.html', {
                    'email': email,
                    'given_name': keycloak_user['firstName'],
                    'family_name': keycloak_user['lastName'],
                })
                ## Send welcome email to new collaborator
                send_email(
                    recipients=email,
                    email_subject='Welcome to the MUSES collaboration',
                    message_body=message_body,
                    template_file='email_frame.tpl.html'
                )
                self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been successfully added to the MUSES collaboration and has been notified by email.''', return_json=False)
                self.finish()
                return
            elif decision == 'reject':
                log.debug(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been rejected.''')
                try:
                    ## Remove demographic information previously submitted
                    db_token = token
                    if not db.remove_demographic_info_record(db_token):
                        log.error(f'''Error removing record associated with token value "{db_token}" from database.''')
                except Exception as e:
                    log.error(f'''Error removing record associated with token value "{db_token}" from database: {e}''')
                ## Render email message
                message_body = renderJinjaTemplate('email_body_rejected.tpl.html', {
                    'email': email,
                    'given_name': keycloak_user['firstName'],
                    'family_name': keycloak_user['lastName'],
                })
                ## Send welcome email to new collaborator
                send_email(
                    recipients=email,
                    email_subject='Request to join the MUSES collaboration was denied',
                    message_body=message_body,
                    template_file='email_frame.tpl.html'
                )
                self.send_response(f'''User "{keycloak_user['firstName']} {keycloak_user['lastName']}" has been notified by email of their rejection.''', return_json=False)
                self.finish()
                return
            else:
                self.send_response(f'''Invalid decision. Supported values are "accept" and "reject".''', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
        except Exception as e:
            log.error(f'''New collaborator approval failed: {e}''')
            self.send_response(str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
    def post(self):
        try:
            email = self.getarg('email').lower() # required
            collaborator_info = self.getarg('collaboratorInfo') # required
            personal_info = self.getarg('personalInfo') # required
        except:
            self.finish()
            return
        try:
            log.debug(f'''New collaboration registration: "{email}"''')
            keycloak_client = keycloak.KeycloakApi()
            keycloak_user = keycloak_client.get_user_info(email=email)
            if not keycloak_user:
                self.send_response({'msg': f'''No MUSES account with email "{email}" found.'''}, http_status_code=global_vars.HTTP_NOT_FOUND, return_json=True)
                self.finish()
                return
            keycloak_user = keycloak_user[0]
            log.debug(f'''Keycloak user info: "{keycloak_user}"''')
            ## Generate hash of email address using secret salt
            email_hash = bcrypt.hashpw(bytes(email, 'utf-8'), bytes(config['server']['hash_salt'], "utf-8"))
            email_hash_urlencoded = parse.quote_plus(email_hash)
            email_urlencoded = parse.quote_plus(email)
            try:
                ## Record demographic information in database
                if not db.insert_demographic_info(personal_info, source='collaborator', token=email_hash):
                    log.error(f'''Error adding info to database: {json.dumps(personal_info)}''')
                ## Record other user information in database
                user_info = {
                    'email': email,
                    'keycloak_id': keycloak_user['id'],
                    'country': personal_info['country'],
                    'affiliation': personal_info['affiliation'],
                    'role': personal_info['role'],
                    'research': personal_info['research'],
                    'working_groups': collaborator_info['workingGroups'],
                    'motivation': collaborator_info['motivation'],
                }
                if not db.insert_user_info(user_info):
                    log.error(f'''Error adding user info to database: {json.dumps(user_info)}''')
            except Exception as e:
                log.error(f'''Error adding info to database: {e}''')
            ## Generate API base path for links
            basePath = get_api_base_path()
            ## Render email message
            message_body = renderJinjaTemplate('email_body_register.tpl.html', {
                'email': email,
                'given_name': keycloak_user['firstName'],
                'family_name': keycloak_user['lastName'],
                'country': escape_html(personal_info['country']),
                'affiliation': escape_html(personal_info['affiliation']),
                'role': escape_html(personal_info['role']),
                'research': escape_html(personal_info['research']),
                'working_groups': collaborator_info['workingGroups'],
                'motivation': escape_html(collaborator_info['motivation']),
                'approve_link': f'''{config['server']['protocol']}://{config['server']['hostName']}/{basePath}/register/collaborator/approve/{email_urlencoded}?token={email_hash_urlencoded}''',
                'reject_link':  f'''{config['server']['protocol']}://{config['server']['hostName']}/{basePath}/register/collaborator/reject/{email_urlencoded}?token={email_hash_urlencoded}''',
            })
            ## Send email to leadership
            send_email(
                recipients=config['email']['recipients'],
                email_subject='New MUSES collaborator request', 
                message_body=message_body,
                template_file='email_frame.tpl.html'
            )
            self.send_response({'msg': 'Collaboration request received!'})
            self.finish()
            return
        except Exception as e:
            log.error(f'''New collaboration registration failed: {e}''')
            self.send_response({'msg': str(e)}, http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=True)
            self.finish()
            return


@authenticated
class JobHandler(BaseHandler):
    def put(self):
        try:
            # Command that the job container will execute
            command = self.getarg('command') # required 
            # Valid run_id value follows the Kubernetes label value constraints:
            #   - must be 63 characters or less (cannot be empty),
            #   - must begin and end with an alphanumeric character ([a-z0-9A-Z]),
            #   - could contain dashes (-), underscores (_), dots (.), and alphanumerics between.
            # See also:
            #   - https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#runId
            #   - https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
            run_id = self.getarg('run_id', default='') # optional
            if run_id and (not isinstance(run_id, str) or run_id != re.sub(r'[^-._a-zA-Z0-9]', "", run_id) or not re.match(r'[a-zA-Z0-9]', run_id)):
                self.send_response('Invalid run_id. Must be 63 characters or less and begin with alphanumeric character and contain only dashes (-), underscores (_), dots (.), and alphanumerics between.', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
            # environment is a list of environment variable names and values like [{'name': 'env1', 'value': 'val1'}]
            environment = self.getarg('environment', default=[]) # optional
            # Number of parallel job containers to run. The containers will execute identical code. Coordination is the 
            # responsibility of the job owner.
            replicas = self.getarg('replicas', default=1) # optional
            # The URL of the git repo to clone
            url = self.getarg('url', default='') # optional
            # The git reference (branch name or commit hash) to be checked out after cloning the git repo
            commit_ref = self.getarg('commit_ref', default='') # optional
        except Exception as e:
            self.send_response(str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        response = kubejob.create_job(
            command=command, 
            run_id=run_id,
            replicas=replicas,
            environment=environment,
            url=url, 
            commit_ref=commit_ref,
        )
        # log.debug(response)
        if response['status'] != global_vars.STATUS_OK:
            self.send_response(response['message'], http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        try:
            timeout = 30
            while timeout > 0:
                results = kubejob.list_jobs(
                    job_id=response['job_id'],
                )
                if results['jobs']:
                    job = construct_job_object(results['jobs'][0])
                    self.send_response(job, indent=2)
                    self.finish()
                    return
                else:
                    timeout -= 1
                    time.sleep(0.300)
            self.send_response("Job creation timed out.", http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        except Exception as e:
            self.send_response(str(e), http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        
    def get(self, job_id=None, property=None):
        # See https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#resourceuri
        valid_properties = {
            'phase': 'phase',
            'results': 'results',
            'parameters': 'parameters',
        }
        response = {}
        # If no job_id is included in the request URL, return a list of jobs. See:
        # UWS Schema: https://www.ivoa.net/documents/UWS/20161024/REC-UWS-1.1-20161024.html#UWSSchema
        if not job_id:
            phase = self.getarg('phase', default='') # optional
            if not phase or phase in global_vars.VALID_JOB_STATUSES:
                results = kubejob.list_jobs()
                if results['status'] != global_vars.STATUS_OK:
                    self.send_response(results['message'], http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
            else:
                response = 'Valid job categories are: {}'.format(global_vars.VALID_JOB_STATUSES)
                self.send_response(response, http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
                self.finish()
                return
            # Construct the UWS-compatible list of job objects
            jobs = []
            for job_info in results['jobs']:
                job = construct_job_object(job_info)
                if not phase or job['phase'] == phase:
                    jobs.append(job)
            self.send_response(jobs, indent=2)
            self.finish()
            return
        # If a job_id is provided but it is invalid, then the request is malformed:
        if not valid_job_id(job_id):
            self.send_response('Invalid job ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
            self.finish()
            return
        # If a property is provided but it is invalid, then the request is malformed:
        elif isinstance(property, str) and property not in valid_properties:
            self.send_response(f'Invalid job property requested. Supported properties are {", ".join([key for key in valid_properties.keys()])}', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
            self.finish()
            return
        else:
            try:
                results = kubejob.list_jobs(
                    job_id=job_id, 
                )
                if results['status'] != global_vars.STATUS_OK:
                    self.send_response(results['message'], http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
                    self.finish()
                    return
                if not results['jobs']:
                    self.send_response(results['message'], http_status_code=global_vars.HTTP_NOT_FOUND)
                    self.finish()
                    return
                job = construct_job_object(results['jobs'][0])
                
                # If a specific job property was requested using an API endpoint 
                # of the form `/job/[job_id]/[property]]`, return that property only.
                if property in valid_properties.keys():
                    self.send_response(job[valid_properties[property]], indent=2)
                else:
                    self.send_response(job, indent=2)
                self.finish()
                return
            except Exception as e:
                response = str(e).strip()
                log.error(response)
                self.send_response(response, http_status_code=global_vars.HTTP_SERVER_ERROR, indent=2)
                self.finish()
                return

    def delete(self, job_id):
        response = kubejob.delete_job(
            job_id=job_id, 
        )
        log.debug(response)
        if response['status'] == global_vars.STATUS_ERROR:
            self.send_response(response['message'], http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
        elif isinstance(response['code'], int) and response['code'] != global_vars.HTTP_OK:
            self.send_response(response['message'], http_status_code=response['code'], return_json=False)
        else:
            self.send_response(response, indent=2)
        self.finish()
        return


class ResultFileHandler(BaseHandler):
    def get(self, job_id=None, result_id=None):
        try:
            # If a job_id is provided but it is invalid, then the request is malformed:
            if not valid_job_id(job_id):
                self.send_response('Invalid job ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
                self.finish()
                return
            # If a result_id is not provided, then the request is malformed:
            if not result_id:
                self.send_response('Invalid result ID.', http_status_code=global_vars.HTTP_BAD_REQUEST, indent=2)
                self.finish()
                return
            try:
                result_idx = int(result_id)
                job_files = kubejob.list_job_output_files(job_id)
                file_path = job_files[result_idx]
            except:
                self.send_response('Result file not found.', http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            if not os.path.isfile(file_path):
                self.send_response('Result file not found.', http_status_code=global_vars.HTTP_NOT_FOUND, return_json=False)
                self.finish()
                return
            # TODO: Consider applying "application/octet-stream" universally given the error rate with the guess_type() function
            content_type, _ = guess_type(file_path)
            if not content_type:
                content_type = "application/octet-stream"
            self.add_header('Content-Type', content_type)
            with open(file_path, 'rb') as source_file:
                self.send_response(source_file.read(), return_json=False)
                self.finish()
                return
        except Exception as e:
            response = str(e).strip()
            log.error(response)
            self.send_response(response, http_status_code=global_vars.HTTP_SERVER_ERROR, indent=2)
            self.finish()
            return


class AccountHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
        html = loader.load("account.html").generate(
            project_name=config['project']['name'],
            profile_url=config['oidc']['profileUrl'],
            user_info=json.loads(self.get_current_user()),
            base_path=config['server']['basePath'],
            base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
        )
        self.send_response(html, return_json=False)
        self.finish()


class AccountTokenCreateHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            user_info = json.loads(self.get_current_user())
            log.debug(json.dumps(user_info, indent=2))
            token = encode_info(user_info).decode(encoding='utf-8')
        except Exception as e:
            self.send_response(f'Error getting user info: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
        html = loader.load("token.html").generate(
            project_name=config['project']['name'],
            profile_url=config['oidc']['profileUrl'],
            user_info=json.loads(self.get_current_user()),
            base_path=config['server']['basePath'],
            base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
            api_base_url=f'''{config['server']['hostName']}/{get_api_base_path()}''',
            ttl=round(config['jwt']['ttlSeconds']/3600.0),
            token=token,
        )
        self.send_response(html, return_json=False)
        self.finish()


class AccountProfileHandler(BaseHandler):

    def get_default_profile_attributes(self):
        return {
            'affiliation',
            'author_name',
            'website',
            'working_group',
            'role',
            'acknowledgements',
        }

    def urlencode_profile_attributes(self, attributes):
        encoded_attributes = {}
        for attribute in attributes:
            encoded_attributes[attribute] = []
            for value in attributes[attribute]:
                encoded_attributes[attribute].append(parse.quote_plus(value.strip()))
        return encoded_attributes

    def get_profile_attributes(self):
        attributes = {}
        profile_attributes = self.get_default_profile_attributes()
        try:
            user_info = json.loads(self.get_current_user())
            keycloak_client = keycloak.KeycloakApi()
            email = user_info['email']
            keycloak_user = keycloak_client.get_user_info(email=email)
            if not keycloak_user:
                return None
            keycloak_user = keycloak_user[0]
            if 'attributes' not in keycloak_user:
                return {}
            for attribute in keycloak_user['attributes']:
                if attribute not in profile_attributes:
                    continue
                attributes[attribute] = []
                for value in keycloak_user['attributes'][attribute]:
                    attributes[attribute].append(parse.unquote_plus(value))
            return attributes
        except Exception as e:
            log.error(f'Error getting user attributes: {e}')
            return None

    @tornado.web.authenticated
    def get(self):
        try:
            user_info = json.loads(self.get_current_user())
            profile_attributes = self.get_profile_attributes()
            assert isinstance(profile_attributes, dict)
        except Exception as e:
            self.send_response(f'Error getting user info: {e}', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        loader = tornado.template.Loader(os.path.join(os.path.dirname(__file__), 'templates'))
        html = loader.load("profile.html").generate(
            css=renderJinjaTemplate('profile_form.tpl.css'),
            project_name=config['project']['name'],
            profile_url=config['oidc']['profileUrl'],
            user_info=user_info,
            profile_attributes = profile_attributes,
            base_path=config['server']['basePath'],
            base_url=f'''{config['server']['hostName']}/{config['server']['basePath']}''',
            api_base_url=f'''{config['server']['hostName']}/{get_api_base_path()}''',
        )
        self.send_response(html, return_json=False)
        self.finish()

    @tornado.web.authenticated
    def post(self):
        try:
            attribute = self.getarg('attribute') # required
            value = self.getarg('value') # required
            value = parse.unquote_plus(value.strip())
            log.debug(value)
            scalar = self.getarg('scalar') # required
            assert isinstance(scalar, bool)
            user_info = json.loads(self.get_current_user())
            user_id = user_info['sub']
            profile_attributes = self.get_profile_attributes()
            keycloak_client = keycloak.KeycloakApi()

            if attribute in profile_attributes:
                ## TODO: What if the attribute is not a list? Can this occur?
                if isinstance(profile_attributes[attribute], list):
                    if value in profile_attributes[attribute]:
                        log.info(f'''Attribute "{attribute}: {value}" already exists. No update required.''')
                        self.send_response(data='Attribute already exists. No update required.')
                        self.finish()
                        return
                    else:
                        if scalar:
                            profile_attributes[attribute] = [value]
                        else:
                            ## Append value to existing attribute.
                            profile_attributes[attribute].append(value)
            else:
                ## Add attribute if it does not exist. Assume it is multi-valued.
                profile_attributes[attribute] = [value]
            log.debug(f'''Updating attribute "{attribute}: {value}"...''')
            keycloak_client.update_user_attributes(user_id=user_id, attributes=self.urlencode_profile_attributes(profile_attributes))
        except Exception as e:
            log.error(f'''Error updating user attributes: {e}''')
            self.finish()
            return
        self.send_response()
        self.finish()
        return

    @tornado.web.authenticated
    def delete(self):
        try:
            attribute = self.getarg('attribute') # required
            value = self.getarg('value') # required
            value = parse.unquote_plus(value.strip())
            log.debug(value)
            scalar = self.getarg('scalar') # required
            assert isinstance(scalar, bool)
            log.debug(f'''Deleting attribute "{attribute}: {value}"...''')
            user_info = json.loads(self.get_current_user())
            user_id = user_info['sub']
            profile_attributes = self.get_profile_attributes()
            keycloak_client = keycloak.KeycloakApi()

            if attribute in profile_attributes:
                if value in profile_attributes[attribute]:
                    profile_attributes[attribute].remove(value)
                    keycloak_client.update_user_attributes(user_id=user_id, attributes=self.urlencode_profile_attributes(profile_attributes))
                else:
                    log.info(f'''Attribute "{attribute}: {value}" not found. No update required.''')
            else:
                log.info(f'''Attribute "{attribute}: {value}" not found. No update required.''')
        except Exception as e:
            log.error(f'''Error updating user attributes: {e}''')
            self.finish()
            return
        self.send_response()
        self.finish()
        return
        

@authenticated
class AccountTokenRefreshHandler(BaseHandler):
    def get(self):
        try:
            auth = self.request.headers.get("Authorization")
            parts = auth.split()
            auth_type = parts[0].lower()
            assert auth_type == 'bearer' and len(parts)==2
            token = parts[1]
        except Exception as e:
            self.send_response(f'Invalid authorization header: {e}', http_status_code=global_vars.HTTP_BAD_REQUEST, return_json=False)
            self.finish()
            return
        response = refresh_token(token)
        new_token = response['token']
        if response['status'] != STATUS_OK:
            self.send_response(f'''Error refreshing token: {response['message']}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
            return
        data = {
            'token': new_token,
        }
        self.send_response(data)
        self.finish()


@authenticated
@allowed_roles(['api_admin'])
class AdminTokenInvalidateHandler(BaseHandler):
    def get(self):
        try:
            denylist = db.get_denylist()
            self.send_response(denylist)
            self.finish()
        except Exception as e:
            self.send_response(f'''Error fetching denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
    def put(self):
        try:
            ## Keycloak user ID
            user_id = self.getarg('uid') # required
        except Exception as e:
            self.finish()
            return
        try:
            db.update_denylist(user_id, 'add')
            self.send_response()
            self.finish()
        except Exception as e:
            self.send_response(f'''Error adding user "{user_id}" to denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()
    def delete(self):
        try:
            ## Keycloak user ID
            user_id = self.getarg('uid') # required
        except Exception as e:
            self.finish()
            return
        try:
            db.update_denylist(user_id, 'remove')
            self.send_response()
            self.finish()
        except Exception as e:
            self.send_response(f'''Error deleting user "{user_id}" from denylist: {e}''', http_status_code=global_vars.HTTP_SERVER_ERROR, return_json=False)
            self.finish()


class KeycloakOAuth2Mixin(OAuth2Mixin):
    '''ref: https://www.tornadoweb.org/en/stable/auth.html'''

    _OAUTH_AUTHORIZE_URL = config['oidc']['authorizeUrl']
    _OAUTH_ACCESS_TOKEN_URL = config['oidc']['tokenUrl']
    _OAUTH_SETTINGS_KEY = {
        'key': config['oidc']['clientId'],
        'secret': config['oidc']['clientSecret'],
    }
    _OAUTH_USERINFO_URL = config['oidc']['userInfoUrl']
    _OAUTH_NO_CALLBACKS = False

    async def get_authenticated_user(self, redirect_uri: str, code: str) -> Dict[str, Any]:
        http = self.get_auth_http_client()
        body = urllib.parse.urlencode(
            {
                "redirect_uri": redirect_uri,
                "code": code,
                "client_id": self._OAUTH_SETTINGS_KEY["key"],
                "client_secret": self._OAUTH_SETTINGS_KEY["secret"],
                "grant_type": "authorization_code",
            }
        )

        response = await http.fetch(
            self._OAUTH_ACCESS_TOKEN_URL,
            method="POST",
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            body=body,
        )
        return escape.json_decode(response.body)


    async def get_user_info(self, access_token):
        http = self.get_auth_http_client()

        response = await http.fetch(
            self._OAUTH_USERINFO_URL,
            method="GET",
            headers={"Authorization": f'''Bearer {access_token}'''},
        )
        return escape.json_decode(response.body)


class LoginHandler(BaseHandler, KeycloakOAuth2Mixin):
    '''ref: https://www.tornadoweb.org/en/stable/auth.html'''
    async def get(self):
        default_path = f"/{config['server']['basePath']}/account"
        default_path = f"/{default_path.strip('/')}"
        if self.get_argument('code', False):
            # default_path = f"/{config['server']['basePath']}/account"
            # next_path = self.get_argument('dest', default_path)
            # if not self.next_path:
            #     self.next_path = default_path
            auth_token = await self.get_authenticated_user(
                redirect_uri=f"{config['oidc']['callbackUrl']}",
                code=self.get_argument('code')
            )
            user_info = await self.get_user_info(
                access_token=auth_token['access_token'],
            )
            if not [group for group in config['oidc']['groups']['allowed'] if group in user_info[config['oidc']['groups']['keyName']]]:
                self.send_response(f'''
                    You are not authorized to access this content.
                    <br><a href="{default_path}/logout">Logout</a>
                    ''', http_status_code=global_vars.HTTP_UNAUTHORIZED, return_json=False)
                self.finish()
                return
            # Save the user and access token
            self.set_secure_cookie("user", json.dumps(user_info))
            try:
                dest_url = self.get_cookie('dest_url')
            except:
                dest_url = default_path
            ## Redirect to the original desired URL or fall back to the default
            self.redirect(dest_url)
        else:
            # self.next_path = self.get_argument('next', default_path)
            self.set_cookie('dest_url', self.get_argument('next', default_path))
            self.authorize_redirect(
                redirect_uri=f"{config['oidc']['callbackUrl']}", #?dest={next_path}",
                client_id=config['oidc']['clientId'],
                client_secret=config['oidc']['clientSecret'],
                scope=config['oidc']['scope'],
                # extra_params={'dest': self.get_argument('next', f"{config['server']['basePath']}/account")},
            )


class LogoutHandler(BaseHandler):
    def get(self):
        # Clear the browser cookie
        self.clear_cookie("user")
        self.redirect(f"/{config['server']['basePath']}")


class ExportCollaboratorsHandler(BaseHandler):
    def get(self):
        client = DiscourseApi()
        group_name = 'Collaborators'
        group_members = client.discourse_get_group_members(group_name=group_name)
        self.send_response(group_members)
        self.finish()
        return


@authenticated
@allowed_roles(['api_admin'])
class ExportCollaboratorsEmailsHandler(BaseHandler):
    def get(self):
        client = DiscourseApi()
        group_name = 'Collaborators'
        group_members = client.discourse_get_group_members(group_name=group_name)
        group_members_with_emails = []
        for member in group_members:
            member_with_emails = member
            member_emails = client.discourse_get_member_emails(username=member['username'])
            member_with_emails['emails'] = member_emails
            group_members_with_emails.append(member_with_emails)
        self.send_response(group_members_with_emails)
        self.finish()
        return


def make_app(app_base_path='/', api_base_path='api', debug=False):
    ## Configure app base path
    app_base_path = f'''/{app_base_path.strip('/')}'''
    if app_base_path == '/':
        app_base_path = ''
    ## Configure API base path
    api_base_path = api_base_path.strip('/')
    settings = {
        "debug": debug,
        "login_url": r"{}/account/login".format(app_base_path),
        "cookie_secret": bcrypt.gensalt().decode('utf-8'),
        "static_path": os.path.join(os.path.dirname(__file__), "assets"),
    }
    return tornado.web.Application(
        [
            (r"{}/account/login".format(app_base_path), LoginHandler),
            (r"{}/account/logout".format(app_base_path), LogoutHandler),
            (r"{}/account/token/create".format(app_base_path), AccountTokenCreateHandler),
            (r"{}/account/profile".format(app_base_path), AccountProfileHandler),
            (r"{}/account/*".format(app_base_path), AccountProfileHandler),
            (r"{}/register/collaborator".format(app_base_path), RegisterCollaboratorFormHandler),
            (r"{}/register/seminar".format(app_base_path), RegisterSeminarFormHandler),
            (r"{}/{}/register/seminar".format(app_base_path, api_base_path), RegisterSeminarHandler),
            (r"{}/{}/register/collaborator".format(app_base_path, api_base_path), RegisterCollaboratorHandler),
            (r"{}/{}/register/collaborator/(.*)/(.*)".format(app_base_path, api_base_path), RegisterCollaboratorHandler),
            (r"{}/{}/register/ce/(.*)/(.*)".format(app_base_path, api_base_path), RegisterCalculationEngineUserHandler),
            (r"{}/{}/uws/job/result/(.*)/(.*)".format(app_base_path, api_base_path), ResultFileHandler),
            (r"{}/{}/uws/job/(.*)/(.*)".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/uws/job/(.*)".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/uws/job".format(app_base_path, api_base_path), JobHandler),
            (r"{}/{}/account/token/refresh".format(app_base_path, api_base_path), AccountTokenRefreshHandler),
            (r"{}/{}/admin/token/invalidate".format(app_base_path, api_base_path), AdminTokenInvalidateHandler),
            (r"{}/account/static/(.*)".format(app_base_path), tornado.web.StaticFileHandler, dict(path=settings['static_path'])),
            (r"{}/{}/export/collaborators".format(app_base_path, api_base_path), ExportCollaboratorsHandler),
            (r"{}/{}/export/collaborators/emails".format(app_base_path, api_base_path), ExportCollaboratorsEmailsHandler),
        ],
        **settings
    )

if __name__ == "__main__":
    try:
        # Wait for database to come online if it is still starting
        waiting_for_db = True
        while waiting_for_db:
            try:
                db.open_db_connection()
                waiting_for_db = False
                db.close_db_connection()
            except:
                log.error('Unable to connect to database. Waiting to try again...')
                time.sleep(5.0)
        ## Create/update database tables
        db.update_db_tables()
    except Exception as e:
        log.error(str(e).strip())
    ## Create TornadoWeb application
    app = make_app(
        app_base_path=config['server']['basePath'],
        api_base_path=config['server']['apiBasePath'],
        debug=config['server']['debug']
    )
    app.listen(int(config['server']['port']))
    log.info(f'''API server listening on port {config['server']['port']} at base path "{config['server']['basePath']}"''')
    if config['devMode']:
        log.debug(json.dumps(config, indent=2))
    tornado.ioloop.IOLoop.current().start()

