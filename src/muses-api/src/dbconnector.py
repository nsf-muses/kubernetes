import mysql.connector
from global_vars import config, log
import os
from datetime import datetime

class DbConnector:
    def __init__(self, mysql_host, mysql_user, mysql_password, mysql_database):
        self.host = mysql_host
        self.user = mysql_user
        self.password = mysql_password
        self.database = mysql_database
        self.cur = None
        self.cnx = None
        self.db_schema_version = 2

    def open_db_connection(self):
        if self.cnx is None or self.cur is None:
            # Open database connection
            self.cnx = mysql.connector.connect(
                host=self.host,
                user=self.user,
                password=self.password,
                database=self.database,
            )
            # Get database cursor object
            self.cur = self.cnx.cursor()

    def close_db_connection(self):
        if self.cnx != None and self.cur != None:
            try:
                # Commit changes to database and close connection
                self.cnx.commit()
                self.cur.close()
                self.cnx.close()
                self.cur = None
                self.cnx = None
            except Exception as e:
                error = str(e).strip()
                self.cur = None
                self.cnx = None
                return False, error

    def parse_sql_commands(self, sql_file):
        msg = ''
        commands = []
        try:
            with open(sql_file) as f:
                dbUpdateSql = f.read()
            #Individual SQL commands must be separated by the custom delimiter '#---'
            for sql_cmd in dbUpdateSql.split('#---'):
                if len(sql_cmd) > 0 and not sql_cmd.isspace():
                    commands.append(sql_cmd)
        except Exception as e:
            msg = str(e).strip()
            log.error(msg)
        return commands

    def update_db_tables(self):
        self.open_db_connection()
        try:
            current_schema_version = 0
            try:
                # Get currently applied database schema version if tables have already been created
                self.cur.execute(
                    "SELECT `schema_version` FROM `meta` LIMIT 1"
                )
                for (schema_version,) in self.cur:
                    current_schema_version = schema_version
                log.info("schema_version taken from meta table")
            except:
                log.info("meta table not found")
            log.info('current schema version: {}'.format(current_schema_version))
            # Update the database schema if the versions do not match
            if current_schema_version < self.db_schema_version:
                # Sequentially apply each DB update until the schema is fully updated
                for db_update_idx in range(current_schema_version+1, self.db_schema_version+1, 1):
                    sql_file = os.path.join(os.path.dirname(__file__), "db_schema_update", "db_schema_update.{}.sql".format(db_update_idx))
                    commands = self.parse_sql_commands(sql_file)
                    for cmd in commands:
                        log.info('Applying SQL command from "{}":'.format(sql_file))
                        log.info(cmd)
                        # Apply incremental update
                        self.cur.execute(cmd)
                    # Update `meta` table to match
                    log.info("Updating `meta` table...")
                    try:
                        self.cur.execute(
                            "INSERT INTO `meta` (`schema_version`) VALUES ({})".format(db_update_idx)
                        )
                    except:
                        self.cur.execute(
                            "UPDATE `meta` SET `schema_version`={}".format(db_update_idx)
                        )
                    self.cur.execute(
                        "SELECT `schema_version` FROM `meta` LIMIT 1"
                    )
                    for (schema_version,) in self.cur:
                        log.info('Current schema version: {}'.format(schema_version))
        except Exception as e:
            log.error(str(e).strip())
        self.close_db_connection()

    def get_denylist(self):
        self.open_db_connection()
        try:
            denylist = []
            self.cur.execute(
                "SELECT `user_id`, `time_added` FROM `user_denylist`"
            )
            for (user_id, time_added) in self.cur:
                denylist.append({
                    'user_id': user_id,
                    'time_added': time_added,
                })
        except Exception as e:
            log.error(str(e).strip())
        self.close_db_connection()
        return denylist

    def update_denylist(self, user_id, action):
        self.open_db_connection()
        try:
            assert action in ['add', 'remove']
            in_denylist = False
            self.cur.execute(
                ("SELECT `user_id` FROM `user_denylist` WHERE `user_id` = %s LIMIT 1"),
                (user_id,)
            )
            for (user_id,) in self.cur:
                in_denylist = True
            if in_denylist and action == 'remove':
                log.debug(f'''Deleting user_id "{user_id}" from `user_denylist`...''')
                self.cur.execute(
                    ("DELETE FROM `user_denylist` WHERE `user_id`=%s"),
                    (user_id,)
                )
            elif not in_denylist and action == 'add':
                log.debug(f'''Inserting user_id "{user_id}" into `user_denylist`...''')
                self.cur.execute(
                    ("INSERT INTO `user_denylist` (`user_id`, `time_added`) VALUES (%s, %s)"),
                    (user_id, datetime.utcnow())
                )
            self.close_db_connection()
        except Exception as e:
            log.error(e)
            self.close_db_connection()
            raise

    def insert_demographic_info(self, info, source='seminar', token=''):
        self.open_db_connection()
        try:
            log.debug("Inserting into `demographics` table...")
            ## Convert the list objects to CSV-formatted text
            if isinstance(info['race'], list):
                race = ','.join(info['race'])
            elif isinstance(info['race'], str):
                race = info['race']
            else:
                race = 'Unknown'
            self.cur.execute(
                (
                    "SELECT `token` FROM `demographics` WHERE `token` = %s LIMIT 1"
                ),
                (
                    token,
                )
            )
            for (id_token,) in self.cur:
                log.warning(f'''Demographic info already captured for token: {token}''')
                return True
            self.cur.execute(
                ('''
                    INSERT INTO `demographics` 
                    (`time`,`token`,`source`,`race`,`ethnicity`,`gender`) 
                    VALUES (%s, %s, %s, %s, %s, %s)
                '''),
                (
                    datetime.utcnow(),
                    token,
                    source,
                    race,
                    info['ethnicity'],
                    info['gender'],
                )
            )
        except Exception as e:
            log.error(str(e).strip())
            self.close_db_connection()
            return False
        self.close_db_connection()
        return True

    def insert_user_info(self, user_info):
        self.open_db_connection()
        try:
            log.debug("Inserting into `user_info` table...")
            table_cols = {
                'email': '',
                'keycloak_id': '',
                'country': '',
                'affiliation': '',
                'role': '',
                'research': '',
                'working_groups': '',
                'motivation': '',
            }
            updated_table_cols = {}
            for col in table_cols:
                if col in user_info:
                    ## Convert the list objects to CSV-formatted text
                    if isinstance(user_info[col], list):
                        updated_table_cols[col] = ','.join(user_info[col])
                    elif isinstance(user_info[col], str):
                        updated_table_cols[col] = user_info[col]
                    else:
                        log.error(f'''Invalid value/type for {col}: {user_info[col]}''')
                        return False
            ## Check email value exists
            if not updated_table_cols['email']:
                log.error(f'''Invalid email''')
                return False
            ## Check if user info assocated with the email address already exists
            self.cur.execute(
                (
                    "SELECT `email` FROM `user_info` WHERE `email` = %s LIMIT 1"
                ),
                (
                    updated_table_cols['email'],
                )
            )
            for (email_val,) in self.cur:
                log.warning(f'''User info already captured for email: {updated_table_cols['email']}''')
                return True
            col_str = '('
            val_str = '('
            val_tuple = ()
            idx = 0
            for key in updated_table_cols:
                idx += 1
                val_tuple = val_tuple + (updated_table_cols[key] ,)
                col_str += f'''`{key}`'''
                val_str += f'''%s'''
                if idx == len(updated_table_cols):
                    col_str += f''')'''
                    val_str += f''')'''
                else:
                    col_str += f''','''
                    val_str += f''','''
            self.cur.execute(
                (f'''
                    INSERT INTO `user_info` 
                    {col_str} 
                    VALUES {val_str}
                '''),
                val_tuple
            )
        except Exception as e:
            log.error(str(e).strip())
            self.close_db_connection()
            return False
        self.close_db_connection()
        return True

    # def remove_demographic_info_token(self, token_to_remove):
    #     self.open_db_connection()
    #     try:
    #         log.debug(f'''Removing token value "{token_to_remove}"...''')
    #         self.cur.execute(
    #             ('''
    #                 UPDATE `demographics` 
    #                 SET `token` = %s
    #                 WHERE `token` = %s
    #             '''),
    #             (
    #                 '',
    #                 token_to_remove,
    #             )
    #         )
    #         if self.cur.rowcount != 1:
    #             log.debug(f'''Error removing token "{token_to_remove}"''')
    #     except Exception as e:
    #         log.error(str(e).strip())
    #         self.close_db_connection()
    #         return False
    #     self.close_db_connection()
    #     return True

    def remove_demographic_info_record(self, token):
        self.open_db_connection()
        try:
            log.debug(f'''Removing record with token value "{token}"...''')
            self.cur.execute(
                ('''
                    DELETE from `demographics` 
                    WHERE token=%s
                '''),
                (
                    token,
                )
            )
        except Exception as e:
            log.error(str(e).strip())
            self.close_db_connection()
            return False
        self.close_db_connection()
        return True
