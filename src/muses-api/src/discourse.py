import requests
import time
import logging

try:
    from global_vars import config
except Exception:
    pass

# Configure logging
logging.basicConfig(format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
log = logging.getLogger(__name__)
try:
    log.setLevel(config['server']['logLevel'].upper())
except Exception as err:
    log.error(err)
    log.setLevel('WARNING')

# Set a default wait duration in seconds to avoid Discourse API rate limits
default_wait_duration = 10


def add_user_to_group(email='', group_name='', skip_group=''):
    client = DiscourseApi()
    group_info = [group for group in client.discourse_get_groups()['groups'] if group['name'] == group_name][0]
    group_id = group_info['id']
    user_entry = client.discourse_get_user_info(email=email)
    if not user_entry:
        raise Exception(f'''No Discourse user associated with email "{email}".''')
    user_info = client.discourse_get_user_info(user_id=user_entry['id'])
    # If skip_group is specified and the user is in skip_group, 
    # do not attempt to add them to the target group.
    if [grp for grp in user_info['groups'] if grp['name'] == skip_group]:
        log.debug(f'''Discourse user "{email}" already in group "{skip_group}"''')
        return False
    if [grp for grp in user_info['groups'] if grp['name'] == group_name]:
        log.debug(f'''Discourse user "{email}" already in group "{group_name}"''')
        return False
    else:
        log.debug(f'''Adding Discourse user "{email}" to group: {group_name}''')
        client.discourse_add_group_members([user_info['username']], group_id)
        return True


class DiscourseApi():
    # ref: https://docs.discourse.org/
    def __init__(self, conf=None):
        try:
            # Hack added for consistency with duplicate `discourse.py` module used by the API server
            conf = config
        except Exception as err:
            log.error(err)
            pass
        self.api_user = conf['discourse']['apiUser']
        self.api_key = conf['discourse']['apiKey']
        self.base_url = conf['discourse']['base_url']

    def discourse_get_users(self, flag='new'):
        log.debug('Running discourse_get_users()...')
        r = requests.request('GET',
                             f'''{self.base_url}/admin/users/list/{flag}.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''{r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_get_users(flag)
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_user_info(self, user_id='', email=''):
        log.debug('Running discourse_get_user_info()...')
        assert user_id or email
        if user_id:
            url = f'''{self.base_url}/admin/users/{user_id}.json'''
        else:
            url = f'''{self.base_url}/admin/users/list/active.json?email={email}'''
        r = requests.request('GET',
                             url,
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
            if email and len(response) > 0:
                response = response[0]
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''discourse_get_user_info response text: {r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_get_user_info(user_id=user_id, email=email)
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_user_emails(self, username):
        log.debug('Running discourse_get_user_emails()...')
        r = requests.request('GET',
                             f'''{self.base_url}/users/{username}/emails.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''discourse_get_user_emails response text: {r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_get_user_emails(username)
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_get_groups(self):
        log.debug('Running discourse_get_groups()...')
        r = requests.request('GET',
                             f'''{self.base_url}/groups.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''{r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_get_groups()
            log.error(r.status_code)
            log.error(r.text)
            response = r
        return response

    def discourse_add_group_members(self, usernames, group_id):
        log.debug('Running discourse_add_group_members()...')
        username_list = ','.join(usernames)
        params = {
            'usernames': username_list,
        }
        r = requests.request('PUT',
                             f'''{self.base_url}/groups/{group_id}/members.json''',
                             json=params,
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        try:
            assert r.status_code == 200
            response = r.json()
        except Exception as err:
            log.error(err)
            if r.status_code == 429:
                log.debug(f'''{r.text}''')
                time.sleep(default_wait_duration)
                return self.discourse_add_group_members(usernames, group_id)
        return response

    def discourse_get_group_members(self, group_name):
        log.debug('Running discourse_get_group_members()...')
        '''ref: https://docs.discourse.org/#operation/listGroupMembers'''
        r = requests.request('GET',
                             f'''{self.base_url}/groups/{group_name}/members.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             },
                             params={
                                 'limit': 999,
                             })
        try:
            assert r.status_code == 200
            # Return only member list. Omit the `owner` and `meta` data
            response = r.json()['members']
        except Exception as err:
            log.error(f'''discourse_get_group_members error: {err}''')
            try:
                # If the call fails due to rate limiting, wait and try again
                if r.status_code == 429:
                    response = r.json()
                    if response['error_type'] == "rate_limit":
                        wait_duration = int(response['extras']['wait_seconds']) + 5
                        log.debug(f'''Rate limited (discourse_get_group_members). Waiting {wait_duration} '''
                                  '''seconds to try again...''')
                        time.sleep(wait_duration)
                        return self.discourse_get_group_members(group_name)
            except Exception as err:
                log.error(err)
                log.error(r.status_code)
                log.error(r.text)
                response = r
        return response

    def discourse_get_member_emails(self, username):
        log.debug('Running discourse_get_member_emails()...')
        '''ref: https://docs.discourse.org/#operation/getUserEmails'''
        log.debug(f'Executing "discourse_get_member_emails(username={username})"...')
        r = requests.request('GET',
                             f'''{self.base_url}/u/{username}/emails.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.error(f'''discourse_get_member_emails 429 error: {r.text}''')
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds']) + 5
            except Exception as err:
                log.error(err)
                # Failed to get a precise wait time
                log.debug('''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_member_emails). Waiting {wait_duration} '''
                      '''seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_member_emails(username)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_post(self, post_id):
        log.debug('Running discourse_get_post()...')
        '''ref: https://docs.discourse.org/#tag/Posts/operation/getPost'''
        log.debug(f'Executing "discourse_get_post(post_id={post_id})"...')
        r = requests.request('GET',
                             f'''{self.base_url}/posts/{post_id}.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds']) + 5
            except Exception as err:
                log.error(err)
                # Failed to get a precise wait time
                log.debug('''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_post). Waiting {wait_duration} '''
                      '''seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_post(post_id=post_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_topic_posts(self, topic_id: int = 0, post_id: int = 0):
        log.debug('Running discourse_get_topic_posts()...')
        '''ref: https://docs.discourse.org/#tag/Topics/operation/getSpecificPostsFromTopic'''
        r = requests.request('GET',
                             f'''{self.base_url}/t/{topic_id}/posts.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             },
                             params={
                                 "post_ids[]": post_id
                             })
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds']) + 5
            except Exception as err:
                log.error(err)
                # Failed to get a precise wait time
                log.debug('''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_topic_posts). Waiting {wait_duration} '''
                      '''seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_topic_posts(topic_id=topic_id, post_id=post_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response

    def discourse_get_topic(self, topic_id: int = 0):
        log.debug('Running discourse_get_topic()...')
        '''ref: https://docs.discourse.org/#tag/Topics/operation/getTopic'''
        r = requests.request('GET',
                             f'''{self.base_url}/t/{topic_id}.json''',
                             headers={
                                 'Api-Username': self.api_user,
                                 'Api-Key': self.api_key,
                             })
        response = []
        if r.status_code == 200:
            response = r.json()
            # log.debug(json.dumps(response))
            return response
        if r.status_code == 429:
            log.debug(r.text)
            wait_duration = 5
            try:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds']) + 5
            except Exception as err:
                log.error(err)
                # Failed to get a precise wait time
                log.debug('''Unable to parse a precise wait time from API rate limiter. Using default.''')
            log.debug(f'''Rate limited (discourse_get_topic). Waiting {wait_duration} '''
                      '''seconds to try again...''')
            time.sleep(wait_duration)
            log.debug('Trying API endpoint again...')
            return self.discourse_get_topic(topic_id=topic_id)
        else:
            log.error(r.status_code)
            log.error(r.text)
        return response
