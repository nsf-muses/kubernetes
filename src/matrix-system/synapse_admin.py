import sys, os
import requests
import json
import time
import datetime
import pathlib


def print_http_reponse(response):
    try:
        print(f'Response: [{response.status_code}]\n{json.dumps(response.json(), indent=2)}')
    except:
        print(f'Response: [{response.status_code}]\n{response.text}')
        
class SynapseAdminApi(object):
    """
    https://github.com/matrix-org/synapse/blob/master/docs/admin_api/
    """

    def __init__(self, conf):
        self.conf = conf
        self.json_header = {
            'Authorization': 'Bearer {}'.format(self.conf['auth_token']),
            'Content-Type': 'application/json',
        }
        self.event_token = ''
        # Get room list
        self.all_rooms = []
        self.list_all_rooms()

    def list_room_media(self, room_id):
        print('Room "{}" media:'.format(room_id))
        response = requests.get(
            'https://{}/_synapse/admin/v1/room/{}/media'.format(self.conf['server_url'], room_id),
            headers=self.json_header
        )
        data = json.loads(response.text)
        print(json.dumps(data, indent=2))
        if response.status_code != 200:
            print('ERROR: {}'.format(response.status_code))
            sys.exit()
        return data

    def purge_history(self, room_id, purge_up_to_ts):
        print('Purging room "{}" history up to {} ...'.format(room_id, purge_up_to_ts))
        response = requests.post(
            'https://{}/_synapse/admin/v1/purge_history/{}'.format(self.conf['server_url'], room_id),
            json={
                'delete_local_events': False,
                'purge_up_to_ts': purge_up_to_ts,
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        print(json.dumps(data, indent=2))
        if response.status_code != 200:
            print('ERROR: {}'.format(response.status_code))
            sys.exit()

    def list_all_rooms(self):
        response = requests.get(
            'https://{}/_synapse/admin/v1/rooms'.format(self.conf['server_url']),
            params={
                'limit': 10000
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        self.all_rooms = data['rooms']
        return data['rooms'], data['total_rooms']

    def deactivate_account(self, user_id):
        response = requests.post(
            f'''https://{self.conf['server_url']}/_synapse/admin/v1/deactivate/{user_id}''',
            params={
                'erase': True
            },
            headers=self.json_header
        )
        print_http_reponse(response)
        return response.status_code == 200

    def event_reports(self, room_id):
        '''
        limit: integer - Is optional but is used for pagination, denoting the maximum number of items to return in this call. Defaults to 100.
        from: integer - Is optional but used for pagination, denoting the offset in the returned results. This should be treated as an opaque value and not explicitly set to anything other than the return value of next_token from a previous call. Defaults to 0.
        dir: string - Direction of event report order. Whether to fetch the most recent first (b) or the oldest first (f). Defaults to b.
        user_id: string - Is optional and filters to only return users with user IDs that contain this value. This is the user who reported the event and wrote the reason.
        room_id: string - Is optional and filters to only return rooms with room IDs that contain this value.

        '''
        response = requests.get(
            'https://{}/_synapse/admin/v1/event_reports'.format(self.conf['server_url']),
            params={
                'limit': 10,
                'room_id': room_id,
                'dir': 'f',
                'from': 0,
                # 'user_id': '',
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        return data

    def get_room_messages(self, room_id, direction = 'f', message_limit = 10):
        '''
        https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-messages

        Parameter 	Type 	Description
        path parameters
        roomId 	string 	Required. The room to get events from.
        query parameters
        from 	string 	Required. The token to start returning events from. This token can be obtained from a prev_batch token returned for each room by the sync API, or from a start or end token returned by a previous request to this endpoint.
        to 	string 	The token to stop returning events at. This token can be obtained from a prev_batch token returned for each room by the sync endpoint, or from a start or end token returned by a previous request to this endpoint.
        dir 	enum 	Required. The direction to return events from. One of: ["b", "f"]
        limit 	integer 	The maximum number of events to return. Default: 10.
        filter 	string 	A JSON RoomEventFilter to filter returned events with.
        '''
        response = requests.get(
            'https://{server_url}/_matrix/client/r0/rooms/{roomId}/messages'.format(server_url=self.conf['server_url'], roomId=room_id),
            params={
                'dir': direction,
                'from': self.event_token,
                # 'to': '',
                'limit': message_limit,
                # 'filter': '',
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        # Set the event_token for the next message history request based on the seeking direction
        if direction == 'f' and 'start' in data:
            self.event_token = data['start']
        elif 'end' in data:
            self.event_token = data['end']
        return data
        
    def get_all_users(self):
        response = requests.get(
            'https://{server_url}/_synapse/admin/v2/users'.format(server_url=self.conf['server_url']),
            params={
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        return data

    def update_user(self, user_id, admin=False):
        response = requests.put(
            f'''https://{self.conf['server_url']}/_synapse/admin/v2/users/{user_id}''',
            json={
                'admin': admin,
            },
            headers=self.json_header
        )
        return response

    def client_sync(self):
        '''
        https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-sync
        '''
        response = requests.get(
            'https://{server_url}/_matrix/client/r0/sync'.format(server_url=self.conf['server_url']),
            params={
            },
            headers=self.json_header
        )
        data = json.loads(response.text)
        print('next_batch: {}'.format(data['next_batch']))
        self.event_token = data['next_batch']
        return data

    def purge_unused_rooms(self):
        all_rooms, total_room_number = self.list_all_rooms()
        room_ids_to_purge = []
        for room_id in all_rooms:
            if room_id['joined_local_members'] == 0:
                room_ids_to_purge.append(room_id['room_id'])
        print(json.dumps(room_ids_to_purge, indent=2))
        print('Rooms to purge: {}'.format(len(room_ids_to_purge)))

        for room_id in room_ids_to_purge:
            print('Purging room: {}...'.format(room_id))
            response = requests.post(
                'https://{}/_synapse/admin/v1/purge_room'.format(self.conf['server_url']),
                json={
                    'room_id': room_id
                },
                headers=self.json_header
            )
            data = json.loads(response.text)
            print(json.dumps(data, indent=2))
            if response.status_code != 200:
                print('ERROR: {}'.format(response.status_code))
                sys.exit()
        print('Room purge complete.')
    
    def download_file(self, url, filename=None, dest_dir=None):
        if not filename:
            filename = url.split('/')[-1]
        if not dest_dir:
            dest_dir = pathlib.Path().resolve()
        os.makedirs(dest_dir, exist_ok=True)
        filepath = os.path.join(dest_dir, filename)
        tmpfilepath = os.path.join(dest_dir, f'''.tmp.{filename}''')
        if os.path.isfile(filepath):
            print(f'''File already downloaded: "{filepath}".''')
        else:
            print(f'''Download file to "{filepath}"...''')
            # NOTE the stream=True parameter below
            with requests.get(url, stream=True) as r:
                r.raise_for_status()
                with open(tmpfilepath, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192): 
                        # If you have chunk encoded response uncomment if
                        # and set chunk_size parameter to None.
                        #if chunk: 
                        f.write(chunk)
            os.rename(tmpfilepath, filepath)
        return filepath


        
if __name__ == "__main__":

    # Import credentials and config from environment variables
    api = SynapseAdminApi({
        'auth_token': os.environ['SYNAPSE_ADMIN_AUTH_TOKEN'],
        'server_url': os.environ['SYNAPSE_ADMIN_SERVER_URL'],
    })
    if api.deactivate_account('@manninga=40illinois.edu:muses.ncsa.illinois.edu'):
        user_info = api.get_all_users()
        print(json.dumps(user_info, indent=2))
    # response = api.update_user('@manninga:antares.ncsa.illinois.edu', admin=True)
    # print_http_reponse(response)
    sys.exit()
    
    # # Get timestamp in milliseconds for history purge
    # milliseconds = int(time.time() * 1000) 
    # print(milliseconds)

    # date_N_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=365)
    # date_N_days_ago_ms = int(date_N_days_ago.timestamp())*1000
    # print(date_N_days_ago_ms)

    # print(datetime.datetime.fromtimestamp(int(milliseconds/1000.0)))
    # print(datetime.datetime.fromtimestamp(int(date_N_days_ago_ms/1000.0)))
    
    with open('all_rooms.json', 'w') as f:
        json.dump(api.all_rooms, f, indent=2)
    
    # Get room_id from room name
    room_name = ''
    room_messages_filename = f'room_messages.{room_name}.json'
    media_info_filename = f'media_info.{room_name}.json'
    room_id = ''
    for room in api.all_rooms:
        if room['name'] == room_name:
            room_id = room['room_id']
    
    # # Sync latest client data
    # ################################################################################
    # sync_data = api.client_sync()
    # with open('sync_data.json', 'w') as f:
    #     json.dump(sync_data, f, indent=2)
    
    # # Download entire room message history
    # ################################################################################
    # all_messages = []
    # room_messages = api.get_room_messages(room_id, direction='b', message_limit=100)
    # print(json.dumps(room_messages, indent=2))
    # while 'chunk' in room_messages and room_messages['chunk']:
    #     print('Fetching messages ({})...'.format(api.event_token))
    #     all_messages.extend(room_messages['chunk'])
    #     room_messages = api.get_room_messages(room_id, direction='b', message_limit=100)
    # all_messages_sorted = sorted(all_messages, key=lambda k: k['origin_server_ts'], reverse=True)
    # with open(room_messages_filename, 'w') as f:
    #     json.dump(all_messages_sorted, f, indent=2)

    # Collect media info
    ################################################################################
    with open(room_messages_filename, 'r') as f:
        room_messages = json.load(f)
    media_info = []
    idx = 0
    for msg in room_messages:
        idx += 1
        try:
            if msg['content']['msgtype'] in ['m.video', 'm.image']:
                # print(json.dumps(msg,indent=2))
                info = {
                    'name': msg['content']['body'],
                    'url': msg['content']['url'].replace('mxc:/', 
                    f'''{os.environ['SYNAPSE_ADMIN_SERVER_URL']}/_matrix/media/r0/download'''),
                    'mimetype': msg['content']['info']['mimetype'],
                    'size': msg['content']['info']['size'],
                    'time': datetime.datetime.fromtimestamp(int(msg['origin_server_ts']/1000.0)).isoformat(),
                    'origin_server_ts': int(msg['origin_server_ts']),
                }
                media_info.append(info)
        except:
            # print('Missing metadata for message: {}'.format(json.dumps(msg, indent=2)))
            pass
    media_info_sorted = sorted(media_info, key=lambda k: k['origin_server_ts'], reverse=True)
    with open(media_info_filename, 'w') as f:
        json.dump(media_info_sorted, f, indent=2)
    
    # Calculate total size of files
    with open(media_info_filename, 'r') as f:
        media_info = json.load(f)
    total_size = 0
    for info in media_info:
        total_size += info['size']
    total_size_gb = total_size/1024**3 # GiB
    print('Total size: {:,.2f} GiB'.format(total_size_gb))


    # Download all media into time-ordered folders
    ################################################################################
    
    with open(media_info_filename, 'r') as f:
        media_list = json.load(f)
    media_info = []
    idx = 0
    for media in media_list:
        idx += 1
        try:
            year = media['time'].split('-')[0]
            month = media['time'].split('-')[1]
            dest_dir = f'''{pathlib.Path().resolve()}/downloads/{room_name}/{year}/{year}{month}'''
            api.download_file(media['url'], filename=media['name'], dest_dir=dest_dir)
        except Exception as e:
            print(f'''Error downloading media {media['name']}: {e}''')
            pass


    sys.exit()
    # Report events
    for room in all_rooms:
        # print('{} ({})'.format(room['name'], room['room_id']))
        # if room['name'] == room_name:
        if room['name']:
            print('{} ({})'.format(room['name'], room['room_id']))
            events = api.event_reports(room['room_id'])
            print('    {}'.format(json.dumps(events, indent=2)))
            # with open('matrix_media.json', 'w') as media_file:
            #     json.dump(media, media_file)
    # List room media URLs
    for room in all_rooms:
        # print('{} ({})'.format(room['name'], room['room_id']))
        if room['name'] == room_name:
            print('{} ({})'.format(room['name'], room['room_id']))
            media = api.list_room_media(room['room_id'])
            print('    {}'.format(json.dumps(media, indent=2)))
            # with open('matrix_media.json', 'w') as media_file:
            #     json.dump(media, media_file)
