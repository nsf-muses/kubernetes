#!/usr/bin/env python3

import os
import sys
import requests
from datetime import datetime
import time
import json
import yaml
import urllib

if __name__ == '__main__':
    '''
    Record the supported IdPs in a JSON file. Parse this file to construct the 
    CILogon authorization URL used in the Keycloak configuration.
    '''

    with open('muses_supported_idps.json', 'r') as idpfile:
        idps = json.load(idpfile)

    cilogon_auth_url = 'https://cilogon.org/authorize?idphint='
    for idx, idp in enumerate(idps):
        print
        comma = ''
        if idx < len(idps)-1:
            comma = ','
        url = urllib.parse.quote_plus(idp['EntityID'])
        cilogon_auth_url += f'''{url}{comma}'''
    print(cilogon_auth_url)
    print(len(idps))
