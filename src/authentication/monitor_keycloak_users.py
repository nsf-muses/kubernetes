#!/usr/bin/env python3

import os
import sys
import requests
import time
import json
from datetime import datetime

'''
This script improves the efficiency of collarborator onboarding. The script polls 
the Keycloak server every few minutes to detect new user registrations. It assumes 
that the new users should be in the Keycloak MUSES realm role `Collaborators` and 
adds them to that role. 

Additionally, new Discourse users are added to the `collaborators` group. A Discourse 
admin must create a new API key for the admin user account at 
https://{forum_base_url}/admin/api/keys

Notifications of actions are posted to a dedicated Matrix room using a service 
account created for the purpose. The access token is obtained by logging into the 
service account and opening Settings > Help > Advanced > Access Token.
'''

try:
    # Import credentials and config from environment variables
    username = os.environ['KEYCLOAK_ADMIN_USER']
    password = os.environ['KEYCLOAK_ADMIN_PASS']
    discourse_api_key = os.environ['DISCOURSE_ADMIN_API_KEY']
    discourse_api_user = os.environ['DISCOURSE_ADMIN_API_USER']
    base_url = 'https://keycloak.muses.ncsa.illinois.edu/auth/admin/realms/MUSES'

    config = {
        'auth_token': '',
        'refresh_token': '',
        'base_url': base_url,
        'username': username,
        'password': password,
        'discourse_api_key': discourse_api_key,
        'discourse_api_user': discourse_api_user,
        'matrix_access_token': os.environ['MATRIX_ACCESS_TOKEN'],
        'matrix_room_id': os.environ['MATRIX_ROOM_ID'],
        'matrix_base_url': os.environ['MATRIX_BASE_URL'],
        'exclude_keycloak_users': os.environ['KEYCLOAK_EXCLUDE_USERS'].split(','),
    }
except:
    print(f'''Error setting configuration.''')
    sys.exit(1)


def login():
    # Login to obtain an auth token
    r = requests.request( 'POST',
        'https://keycloak.muses.ncsa.illinois.edu/auth/realms/master/protocol/openid-connect/token',
        data={
            'client_id': 'admin-cli',
            'grant_type': 'password',
            'username': config['username'],
            'password': config['password'],
        },
    )
    # Store the JWT auth token
    try:
        assert r.status_code == 200
        response = r.json()
        # print(json.dumps(response, indent=2))
        config['auth_token'] = response['access_token']
        config['refresh_token'] = response['refresh_token']
    except:
        print(f'''Error authenticating user "{config['username']}". Exiting.''')
        print(r.status_code)
        print(r.text)
        sys.exit(1)
    return config['auth_token']


def exchange_refresh_token():
    # Refresh auth token using refresh token
    if not config['refresh_token']:
        login()
        return config['auth_token']
    r = requests.request( 'POST',
        'https://keycloak.muses.ncsa.illinois.edu/auth/realms/master/protocol/openid-connect/token',
        data={
            'client_id': 'admin-cli',
            'grant_type': 'refresh_token',
            'refresh_token': config['refresh_token'],
        },
    )
    try:
        if r.status_code == 400:
            print(r.status_code)
            print(r.text)
            login()
            return config['auth_token']
        assert r.status_code == 200
        response = r.json()
    except:
        print('Error refreshing auth token. Exiting.')
        sys.exit(1)
    config['auth_token'] = response['access_token']
    return config['auth_token']


def list_users():
    params = {}

    exchange_refresh_token()
    response = requests.request( 'GET',
        f'''{config['base_url']}/users''',
        params=params,
        headers={'Authorization': 'Bearer {}'.format(config['auth_token'])},
    )
    try:
        response = response.json()
    except:
        print(response.text)
    return response


def apply_role_mapping_to_user(user_id, role_id='654fc8c4-01c8-4525-a8fe-b717555c048f', role_name='Collaborators', realm='MUSES'):
    params = [{
        "id": role_id,
        "name": role_name,
        "description": "{"+role_name+"}",
        "composite": False,
        "clientRole": False,
        "containerId": realm,
    }]

    exchange_refresh_token()
    r = requests.request( 'POST',
        f'''{config['base_url']}/users/{user_id}/role-mappings/realm''',
        json=params,
        headers={'Authorization': 'Bearer {}'.format(config['auth_token'])},
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        print(r.status_code)
        print(r.text)
        response = r
    return response

def get_user_role_mapping(user_id, realm='MUSES'):
    assert user_id and realm
    response = requests.request( 'GET',
        f'''{config['base_url']}/users/{user_id}/role-mappings''',
        headers={'Authorization': 'Bearer {}'.format(config['auth_token'])},
    )
    try:
        assert response.status_code in [200, 204]
        response = response.json()
    except:
        print(f'''Error getting "{user_id}" realm role mappings.''')
        print(f'''[{response.status_code}] {response.text}''')
    return response

def send_alert(alert_msg, room_id=config['matrix_room_id'], access_token=config['matrix_access_token'], base_url=config['matrix_base_url']):
    params = {
        "msgtype": 'm.text',
        "body": alert_msg,
    }

    r = requests.request( 'POST',
        f'''{base_url}/_matrix/client/r0/rooms/{room_id}/send/m.room.message?access_token={access_token}''',
        json=params,
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_get_groups(base_url='https://forum.musesframework.io'):

    r = requests.request( 'GET',
        f'''{base_url}/groups.json''',
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_get_groups(base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_list_group_members(group_name, base_url='https://forum.musesframework.io'):

    r = requests.request( 'GET',
        f'''{base_url}/groups/{group_name}/members.json''',
        headers={
            'Api-Key': config['discourse_api_key'],
            'Api-Username': config['discourse_api_user'],
        },
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_list_group_members(group_name, base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_add_group_members(usernames, group_id, base_url='https://forum.musesframework.io'):
    username_list = ','.join(usernames)
    params = {
        'usernames': username_list,
    }

    r = requests.request( 'PUT',
        f'''{base_url}/groups/{group_id}/members.json''',
        json=params,
        headers={
            'Api-Key': config['discourse_api_key'],
            'Api-Username': config['discourse_api_user'],
        },
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_add_group_members(usernames, group_id, base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_get_user_info(user_id='', base_url='https://forum.musesframework.io'):
    r = requests.request( 'GET',
        f'''{base_url}/admin/users/{user_id}.json''',
        headers={
            'Api-Key': config['discourse_api_key'],
            'Api-Username': config['discourse_api_user'],
        },
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_get_user_info(user_id, base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_get_user_emails(username='', base_url='https://forum.musesframework.io'):
    r = requests.request( 'GET',
        f'''{base_url}/users/{username}/emails.json''',
        headers={
            'Api-Key': config['discourse_api_key'],
            'Api-Username': config['discourse_api_user'],
        },
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_get_user_emails(username, base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

def discourse_get_users(flag='new', base_url='https://forum.musesframework.io'):
    r = requests.request( 'GET',
        f'''{base_url}/admin/users/list/{flag}.json''',
        headers={
            'Api-Key': config['discourse_api_key'],
            'Api-Username': config['discourse_api_user'],
        },
    )
    try:
        assert r.status_code == 200
        response = r.json()
    except:
        try:
            ## If the call fails due to rate limiting, wait and try again
            if r.status_code == 429:
                response = r.json()
                if response['error_type'] == "rate_limit":
                    wait_duration = int(response['extras']['wait_seconds'])+5
                    print(f'''Rate limited. Waiting {wait_duration} seconds to try again...''')
                    time.sleep(wait_duration)
                    return discourse_get_users(flag, base_url)
        except:
            print(r.status_code)
            print(r.text)
            response = r
        print(r.status_code)
        print(r.text)
        response = r
    return response

if __name__ == '__main__':
    
    print(f'''{time.asctime( time.localtime(time.time()) )}: Begin polling for user info updates...''')
    send_alert('Monitoring Keycloak users.')
    while True:
        try:
            with open('user_list.json', 'r') as userfile:
                cur_user_info = json.load(userfile)
        except Exception as e:
            print(f'''WARNING: Cannot load user info from file.''')
            cur_user_info = []

        try:
            ## Get latest Keycloak user info
            ##
            keycloak_user_info = list_users()
            ## Search for new users and add roles and groups if necessary
            ##
            for keycloak_user in keycloak_user_info:
                print(f'''Inspecting Keycloak user: {keycloak_user['email']}''')
                keycloak_user_email = keycloak_user['email']
                role_mapping = get_user_role_mapping(keycloak_user['id'])
                if not [mapping for mapping in role_mapping['realmMappings'] if mapping['name'] == 'Collaborators'] and keycloak_user['username'] not in config['exclude_keycloak_users']:
                    msg = f'''{time.asctime( time.localtime(time.time()) )} User "{keycloak_user['username']}" is new. Applying Collaborator role to new user...'''
                    print(msg)
                    send_alert(msg)
                    apply_role_mapping_to_user(keycloak_user['id'])
                else:
                    continue

            ## Get Discourse group information
            ##
            group_name = 'collaborators'
            print(f'''Querying group info: {group_name}''')
            group_info = [group for group in discourse_get_groups()['groups'] if group['name'] == group_name][0]
            group_id = group_info['id']
            
            print(f'''Listing group members: {group_name}''')
            group_members = discourse_list_group_members(group_name)
            ## Store group info to a file
            
            print(f'''Storing group info to file: {group_name}''')
            with open(f'''discourse_group.{group_name}.json''', 'w') as group_member_file:
                json.dump(group_members, group_member_file, indent=2)
            ## Get new Discourse users
            print(f'''Querying new Discourse users...''')
            new_discourse_users = discourse_get_users()
            
            for new_discourse_user in new_discourse_users:
                try:
                    if new_discourse_user['username'] in ['system', 'discobot', 'admin', 'tamanning']:
                        continue
                    print(f'''Querying Discourse user: {new_discourse_user['username']}''')
                    new_discourse_user_info = discourse_get_user_info(user_id=new_discourse_user['id'])
                    new_discourse_user_email = discourse_get_user_emails(new_discourse_user_info['username'])['email']
                    ## If the new Discourse user is not in the group, then add them.
                    if not [grp for grp in new_discourse_user_info['groups'] if grp['name'] == group_name]:
                        print(f'''Adding Discourse user to group: {group_name}''')
                        discourse_add_group_members([new_discourse_user['username']], group_id)
                        send_alert(f'''Added Discourse user {new_discourse_user['username']} to group: {group_name}''')
                    else:
                        print(f'''Discourse user {new_discourse_user['username']} is already a member of Discourse group: {group_name}''')
                except Exception as e:
                    print(f'''ERROR: Cannot add Discourse user {new_discourse_user['username']} to group. Msg: {e}''')
                    
            ## Write updated Keycloak user info to file
            keycloak_user_info = list_users()
            with open('user_list.json', 'w') as userfile:
                json.dump(keycloak_user_info, userfile, indent=2)
        except Exception as e:
            print(f'''ERROR: {e}''')
            sys.exit(1)
        time.sleep(240.0)

    
