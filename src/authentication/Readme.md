
1. Build the Docker image:

    ```sh
    $ docker push registry.gitlab.com/nsf-muses/deployment/kubernetes/workhorse:latest 
    $ docker build -t registry.gitlab.com/nsf-muses/deployment/kubernetes/workhorse:latest .
    ```

1. Copy `workhorse_deployment.yaml` to `.workhorse_deployment.yaml` and add the secret values for authentication.

1. Deploy the workhorse to run the monitor:

    ```sh
    $ kubectl apply -f .workhorse_deployment.yaml 
    ```
