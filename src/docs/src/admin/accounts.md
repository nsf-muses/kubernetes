# MUSES accounts

## Downloading MUSES account profile data

The script `scripts/keycloak_standalone.py` includes a function to compile all the MUSES account profile data into a file. First, set the Keycloak admin credentials as environment variables (prefix a space to `KEYCLOAK_PASS` to prevent storing in shell history):

```python
export KEYCLOAK_USER=*****
 export KEYCLOAK_PASS=*****
```

Then, in the `main` execution block of the Python script, run `compile_collaborator_info_with_profile_attributes()`:

```bash
api = KeycloakApi()
compile_collaborator_info_with_profile_attributes()
```

The member info will be saved it `/tmp/collaborator_info.json`.
