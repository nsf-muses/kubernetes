Modular Unified Solver of the Equation of State (MUSES)
=========================================================

This repository holds the Kubernetes deployment configuration for the web services supporting the Modular Unified Solver of the Equation of State (MUSES) project.

.. toctree::
   :maxdepth: 2
   :caption: User Manual
   :glob:

   user/overview
   user/*

.. toctree::
   :maxdepth: 1
   :caption: Developer Guide
   :glob:

   developer/overview
   developer/*

.. toctree::
   :maxdepth: 1
   :caption: Infrastructure and Deployment
   :glob:

   deploy/overview
   deploy/administration
   deploy/storage
   deploy/sealed-secrets
   deploy/*


.. toctree::
   :maxdepth: 1
   :caption: Administration
   :glob:

   admin/overview
   admin/*
