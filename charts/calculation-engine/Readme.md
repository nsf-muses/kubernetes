# MUSES Calculation Engine

## Porting the Docker Compose deployment to Kubernetes

Services:

- API server: StatefulSet, multiple replicas
- Celery workers: StatefulSet, multiple replicas
- DinD daemon: define as sidecar container for Celery workers
- Setup permissions: omit; use initContainers when necessary
- Celery beat: StatefulSet, single replica
- Flower: StatefulSet, single replica
- Database: Third-party Helm chart
- Object store: Third-party Helm chart
- RabbitMQ: Third-party Helm chart
- API proxy: omit; satisfied by existing ingress controller
- S3 NGINX gateway: omit; not appropriate for production deployment
